/********************************************************************************
** Form generated from reading UI file 'key.ui'
**
** Created by: Qt User Interface Compiler version 5.12.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_KEY_H
#define UI_KEY_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_key
{
public:
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;
    QPushButton *pushButton_4;
    QPushButton *pushButton_5;
    QPushButton *pushButton_6;
    QPushButton *pushButton_7;
    QPushButton *pushButton_8;
    QPushButton *pushButton_9;
    QPushButton *pushButton_10;
    QPushButton *pushButton_11;
    QPushButton *pushButton_12;
    QLineEdit *lineEdit;

    void setupUi(QWidget *key)
    {
        if (key->objectName().isEmpty())
            key->setObjectName(QString::fromUtf8("key"));
        key->resize(414, 438);
        pushButton = new QPushButton(key);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(80, 120, 50, 50));
        QFont font;
        font.setFamily(QString::fromUtf8("HarmonyOS Sans SC Medium"));
        font.setPointSize(20);
        pushButton->setFont(font);
        pushButton_2 = new QPushButton(key);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
        pushButton_2->setGeometry(QRect(160, 120, 50, 50));
        pushButton_2->setStyleSheet(QString::fromUtf8("font: 57 20pt \"HarmonyOS Sans SC Medium\";"));
        pushButton_3 = new QPushButton(key);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));
        pushButton_3->setGeometry(QRect(250, 120, 50, 50));
        pushButton_3->setStyleSheet(QString::fromUtf8("font: 57 20pt \"HarmonyOS Sans SC Medium\";"));
        pushButton_4 = new QPushButton(key);
        pushButton_4->setObjectName(QString::fromUtf8("pushButton_4"));
        pushButton_4->setGeometry(QRect(80, 190, 50, 50));
        pushButton_4->setStyleSheet(QString::fromUtf8("font: 57 20pt \"HarmonyOS Sans SC Medium\";"));
        pushButton_5 = new QPushButton(key);
        pushButton_5->setObjectName(QString::fromUtf8("pushButton_5"));
        pushButton_5->setGeometry(QRect(160, 190, 50, 50));
        pushButton_5->setStyleSheet(QString::fromUtf8("font: 57 20pt \"HarmonyOS Sans SC Medium\";"));
        pushButton_6 = new QPushButton(key);
        pushButton_6->setObjectName(QString::fromUtf8("pushButton_6"));
        pushButton_6->setGeometry(QRect(250, 190, 50, 50));
        pushButton_6->setStyleSheet(QString::fromUtf8("font: 57 20pt \"HarmonyOS Sans SC Medium\";"));
        pushButton_7 = new QPushButton(key);
        pushButton_7->setObjectName(QString::fromUtf8("pushButton_7"));
        pushButton_7->setGeometry(QRect(80, 270, 50, 50));
        pushButton_7->setStyleSheet(QString::fromUtf8("font: 57 20pt \"HarmonyOS Sans SC Medium\";"));
        pushButton_8 = new QPushButton(key);
        pushButton_8->setObjectName(QString::fromUtf8("pushButton_8"));
        pushButton_8->setGeometry(QRect(160, 270, 50, 50));
        pushButton_8->setStyleSheet(QString::fromUtf8("font: 57 20pt \"HarmonyOS Sans SC Medium\";"));
        pushButton_9 = new QPushButton(key);
        pushButton_9->setObjectName(QString::fromUtf8("pushButton_9"));
        pushButton_9->setGeometry(QRect(250, 270, 50, 50));
        pushButton_9->setStyleSheet(QString::fromUtf8("font: 57 20pt \"HarmonyOS Sans SC Medium\";"));
        pushButton_10 = new QPushButton(key);
        pushButton_10->setObjectName(QString::fromUtf8("pushButton_10"));
        pushButton_10->setGeometry(QRect(250, 340, 50, 50));
        pushButton_10->setStyleSheet(QString::fromUtf8("font: 57 20pt \"HarmonyOS Sans SC Medium\";"));
        pushButton_11 = new QPushButton(key);
        pushButton_11->setObjectName(QString::fromUtf8("pushButton_11"));
        pushButton_11->setGeometry(QRect(80, 340, 50, 50));
        pushButton_11->setStyleSheet(QString::fromUtf8("font: 57 20pt \"HarmonyOS Sans SC Medium\";"));
        pushButton_12 = new QPushButton(key);
        pushButton_12->setObjectName(QString::fromUtf8("pushButton_12"));
        pushButton_12->setGeometry(QRect(160, 340, 50, 50));
        pushButton_12->setStyleSheet(QString::fromUtf8("font: 57 18pt \"HarmonyOS Sans SC Medium\";"));
        lineEdit = new QLineEdit(key);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        lineEdit->setGeometry(QRect(60, 50, 250, 50));

        retranslateUi(key);

        QMetaObject::connectSlotsByName(key);
    } // setupUi

    void retranslateUi(QWidget *key)
    {
        key->setWindowTitle(QApplication::translate("key", "Form", nullptr));
        pushButton->setText(QApplication::translate("key", "1", nullptr));
        pushButton_2->setText(QApplication::translate("key", "2", nullptr));
        pushButton_3->setText(QApplication::translate("key", "3", nullptr));
        pushButton_4->setText(QApplication::translate("key", "4", nullptr));
        pushButton_5->setText(QApplication::translate("key", "5", nullptr));
        pushButton_6->setText(QApplication::translate("key", "6", nullptr));
        pushButton_7->setText(QApplication::translate("key", "7", nullptr));
        pushButton_8->setText(QApplication::translate("key", "8", nullptr));
        pushButton_9->setText(QApplication::translate("key", "9", nullptr));
        pushButton_10->setText(QApplication::translate("key", "ok", nullptr));
        pushButton_11->setText(QApplication::translate("key", "0", nullptr));
        pushButton_12->setText(QApplication::translate("key", "DEL", nullptr));
    } // retranslateUi

};

namespace Ui {
    class key: public Ui_key {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_KEY_H
