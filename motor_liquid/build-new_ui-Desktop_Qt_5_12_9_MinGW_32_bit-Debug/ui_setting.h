/********************************************************************************
** Form generated from reading UI file 'setting.ui'
**
** Created by: Qt User Interface Compiler version 5.12.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SETTING_H
#define UI_SETTING_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QFrame>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_setting
{
public:
    QPushButton *pushButton;
    QLabel *label_2;
    QLabel *label_3;
    QFrame *frame;
    QLabel *label_16;
    QLabel *label_17;
    QFrame *frame_2;
    QLabel *label_18;
    QLabel *label_19;
    QFrame *frame_4;
    QLabel *label_22;
    QLineEdit *lineEdit_6;
    QLineEdit *lineEdit_7;
    QLabel *label_25;
    QLabel *label_26;
    QLabel *label_27;
    QLabel *label_28;
    QFrame *frame_7;
    QLabel *label_39;
    QLineEdit *lineEdit_12;
    QLineEdit *lineEdit_13;
    QLabel *label_40;
    QLabel *label_41;
    QLabel *label_42;
    QLabel *label_43;
    QFrame *frame_8;
    QLabel *label_49;
    QLineEdit *lineEdit_16;
    QLineEdit *lineEdit_17;
    QLabel *label_50;
    QLabel *label_51;
    QLabel *label_52;
    QLabel *label_53;
    QFrame *frame_3;
    QLabel *label_20;
    QLabel *label_21;
    QPushButton *pushButton_2;
    QFrame *frame_5;
    QLabel *label_23;
    QLineEdit *lineEdit_18;
    QLineEdit *lineEdit_19;
    QLabel *label_54;
    QLabel *label_55;
    QLabel *label_56;
    QLabel *label_57;
    QFrame *frame_6;
    QLabel *label_24;
    QLineEdit *lineEdit_20;
    QLineEdit *lineEdit_21;
    QLabel *label_58;
    QLabel *label_59;
    QLabel *label_60;
    QLabel *label_61;
    QFrame *frame_9;
    QLabel *label_62;
    QLineEdit *lineEdit_22;
    QLineEdit *lineEdit_23;
    QLabel *label_63;
    QLabel *label_64;
    QLabel *label_65;
    QLabel *label_66;
    QFrame *frame_10;
    QLabel *label_67;
    QLineEdit *lineEdit_24;
    QLineEdit *lineEdit_25;
    QLabel *label_68;
    QLabel *label_69;
    QLabel *label_70;
    QLabel *label_71;
    QFrame *frame_11;
    QLabel *label_72;
    QLineEdit *lineEdit_26;
    QLineEdit *lineEdit_27;
    QLabel *label_73;
    QLabel *label_74;
    QLabel *label_75;
    QLabel *label_76;
    QFrame *frame_12;
    QLabel *label_5;
    QLabel *label_4;
    QFrame *frame_13;
    QLabel *label_8;
    QLabel *label_11;
    QLineEdit *lineEdit_5;
    QLabel *label_7;
    QLabel *label_13;
    QLabel *label_15;
    QLabel *label_10;
    QLineEdit *lineEdit_3;
    QLabel *label_14;
    QLineEdit *lineEdit_4;
    QLabel *label_6;
    QLabel *label_12;
    QLineEdit *lineEdit_2;
    QLabel *label;
    QLabel *label_9;
    QLineEdit *lineEdit;

    void setupUi(QWidget *setting)
    {
        if (setting->objectName().isEmpty())
            setting->setObjectName(QString::fromUtf8("setting"));
        setting->resize(1920, 1080);
        setting->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 0, 0);"));
        pushButton = new QPushButton(setting);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(1798, 21, 82, 58));
        pushButton->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 0, 0);"));
        label_2 = new QLabel(setting);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(40, 34, 50, 31));
        label_2->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_3 = new QLabel(setting);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(120, 30, 443, 36));
        QFont font;
        font.setFamily(QString::fromUtf8("HarmonyOS Sans SC Medium"));
        font.setPointSize(22);
        label_3->setFont(font);
        label_3->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        frame = new QFrame(setting);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setGeometry(QRect(39, 415, 258, 600));
        frame->setStyleSheet(QString::fromUtf8("background-color: rgba(42, 47, 59, 1);"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        label_16 = new QLabel(frame);
        label_16->setObjectName(QString::fromUtf8("label_16"));
        label_16->setGeometry(QRect(67, 364, 124, 31));
        QFont font1;
        font1.setFamily(QString::fromUtf8("HarmonyOS Sans SC Medium"));
        font1.setPointSize(18);
        label_16->setFont(font1);
        label_16->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_16->setAlignment(Qt::AlignCenter);
        label_17 = new QLabel(frame);
        label_17->setObjectName(QString::fromUtf8("label_17"));
        label_17->setGeometry(QRect(69, 219, 120, 115));
        frame_2 = new QFrame(setting);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        frame_2->setGeometry(QRect(307, 415, 1581, 206));
        frame_2->setStyleSheet(QString::fromUtf8("background-color: rgba(42, 47, 59, 1);"));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        label_18 = new QLabel(frame_2);
        label_18->setObjectName(QString::fromUtf8("label_18"));
        label_18->setGeometry(QRect(61, 40, 136, 94));
        label_19 = new QLabel(frame_2);
        label_19->setObjectName(QString::fromUtf8("label_19"));
        label_19->setGeometry(QRect(99, 154, 59, 30));
        label_19->setFont(font1);
        label_19->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_19->setAlignment(Qt::AlignCenter);
        frame_4 = new QFrame(frame_2);
        frame_4->setObjectName(QString::fromUtf8("frame_4"));
        frame_4->setGeometry(QRect(251, 10, 407, 186));
        frame_4->setStyleSheet(QString::fromUtf8("background-color: rgb(61, 67, 84);"));
        frame_4->setFrameShape(QFrame::StyledPanel);
        frame_4->setFrameShadow(QFrame::Raised);
        label_22 = new QLabel(frame_4);
        label_22->setObjectName(QString::fromUtf8("label_22"));
        label_22->setGeometry(QRect(20, 21, 55, 27));
        label_22->setFont(font1);
        label_22->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_22->setAlignment(Qt::AlignCenter);
        lineEdit_6 = new QLineEdit(frame_4);
        lineEdit_6->setObjectName(QString::fromUtf8("lineEdit_6"));
        lineEdit_6->setGeometry(QRect(76, 63, 240, 50));
        lineEdit_6->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 0, 0);\n"
"font: 57 20pt \"HarmonyOS Sans SC Medium\";\n"
"color: rgb(255, 255, 255);\n"
"border-top-color: rgb(0, 0, 0);\n"
""));
        lineEdit_6->setAlignment(Qt::AlignCenter);
        lineEdit_7 = new QLineEdit(frame_4);
        lineEdit_7->setObjectName(QString::fromUtf8("lineEdit_7"));
        lineEdit_7->setGeometry(QRect(76, 123, 240, 50));
        lineEdit_7->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 0, 0);\n"
"color: rgb(255, 255, 255);\n"
"font: 57 20pt \"HarmonyOS Sans SC Medium\";"));
        lineEdit_7->setAlignment(Qt::AlignCenter);
        label_25 = new QLabel(frame_4);
        label_25->setObjectName(QString::fromUtf8("label_25"));
        label_25->setGeometry(QRect(20, 81, 41, 20));
        QFont font2;
        font2.setFamily(QString::fromUtf8("HarmonyOS Sans SC Medium"));
        font2.setPointSize(12);
        label_25->setFont(font2);
        label_25->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_25->setAlignment(Qt::AlignCenter);
        label_26 = new QLabel(frame_4);
        label_26->setObjectName(QString::fromUtf8("label_26"));
        label_26->setGeometry(QRect(20, 141, 41, 20));
        label_26->setFont(font2);
        label_26->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_26->setAlignment(Qt::AlignCenter);
        label_27 = new QLabel(frame_4);
        label_27->setObjectName(QString::fromUtf8("label_27"));
        label_27->setGeometry(QRect(331, 81, 34, 22));
        label_27->setFont(font2);
        label_27->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_27->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        label_28 = new QLabel(frame_4);
        label_28->setObjectName(QString::fromUtf8("label_28"));
        label_28->setGeometry(QRect(331, 140, 34, 20));
        label_28->setFont(font2);
        label_28->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_28->setAlignment(Qt::AlignCenter);
        frame_7 = new QFrame(frame_2);
        frame_7->setObjectName(QString::fromUtf8("frame_7"));
        frame_7->setGeometry(QRect(680, 10, 407, 186));
        frame_7->setStyleSheet(QString::fromUtf8("background-color: rgb(61, 67, 84);\n"
""));
        frame_7->setFrameShape(QFrame::StyledPanel);
        frame_7->setFrameShadow(QFrame::Raised);
        label_39 = new QLabel(frame_7);
        label_39->setObjectName(QString::fromUtf8("label_39"));
        label_39->setGeometry(QRect(21, 21, 108, 27));
        label_39->setFont(font1);
        label_39->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_39->setAlignment(Qt::AlignCenter);
        lineEdit_12 = new QLineEdit(frame_7);
        lineEdit_12->setObjectName(QString::fromUtf8("lineEdit_12"));
        lineEdit_12->setGeometry(QRect(76, 63, 240, 50));
        lineEdit_12->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 0, 0);\n"
"border-top-color: rgb(0, 0, 0);\n"
"color: rgb(255, 255, 255);\n"
"font: 57 20pt \"HarmonyOS Sans SC Medium\";"));
        lineEdit_12->setAlignment(Qt::AlignCenter);
        lineEdit_13 = new QLineEdit(frame_7);
        lineEdit_13->setObjectName(QString::fromUtf8("lineEdit_13"));
        lineEdit_13->setGeometry(QRect(76, 123, 240, 50));
        lineEdit_13->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 0, 0);\n"
"color: rgb(255, 255, 255);\n"
"font: 57 20pt \"HarmonyOS Sans SC Medium\";"));
        lineEdit_13->setAlignment(Qt::AlignCenter);
        label_40 = new QLabel(frame_7);
        label_40->setObjectName(QString::fromUtf8("label_40"));
        label_40->setGeometry(QRect(20, 81, 41, 20));
        label_40->setFont(font2);
        label_40->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_40->setAlignment(Qt::AlignCenter);
        label_41 = new QLabel(frame_7);
        label_41->setObjectName(QString::fromUtf8("label_41"));
        label_41->setGeometry(QRect(20, 141, 41, 20));
        label_41->setFont(font2);
        label_41->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_41->setAlignment(Qt::AlignCenter);
        label_42 = new QLabel(frame_7);
        label_42->setObjectName(QString::fromUtf8("label_42"));
        label_42->setGeometry(QRect(331, 81, 65, 20));
        label_42->setFont(font2);
        label_42->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_42->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        label_43 = new QLabel(frame_7);
        label_43->setObjectName(QString::fromUtf8("label_43"));
        label_43->setGeometry(QRect(331, 140, 65, 20));
        label_43->setFont(font2);
        label_43->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_43->setAlignment(Qt::AlignCenter);
        frame_8 = new QFrame(frame_2);
        frame_8->setObjectName(QString::fromUtf8("frame_8"));
        frame_8->setGeometry(QRect(1140, 10, 407, 186));
        frame_8->setStyleSheet(QString::fromUtf8("background-color: rgb(61, 67, 84);"));
        frame_8->setFrameShape(QFrame::StyledPanel);
        frame_8->setFrameShadow(QFrame::Raised);
        label_49 = new QLabel(frame_8);
        label_49->setObjectName(QString::fromUtf8("label_49"));
        label_49->setGeometry(QRect(20, 21, 55, 27));
        label_49->setFont(font1);
        label_49->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_49->setAlignment(Qt::AlignCenter);
        lineEdit_16 = new QLineEdit(frame_8);
        lineEdit_16->setObjectName(QString::fromUtf8("lineEdit_16"));
        lineEdit_16->setGeometry(QRect(76, 63, 240, 50));
        lineEdit_16->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 0, 0);\n"
"border-top-color: rgb(0, 0, 0);\n"
"color: rgb(255, 255, 255);\n"
"font: 57 20pt \"HarmonyOS Sans SC Medium\";"));
        lineEdit_16->setAlignment(Qt::AlignCenter);
        lineEdit_17 = new QLineEdit(frame_8);
        lineEdit_17->setObjectName(QString::fromUtf8("lineEdit_17"));
        lineEdit_17->setGeometry(QRect(76, 123, 240, 50));
        lineEdit_17->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 0, 0);\n"
"color: rgb(255, 255, 255);\n"
"font: 57 20pt \"HarmonyOS Sans SC Medium\";"));
        lineEdit_17->setAlignment(Qt::AlignCenter);
        label_50 = new QLabel(frame_8);
        label_50->setObjectName(QString::fromUtf8("label_50"));
        label_50->setGeometry(QRect(20, 81, 41, 20));
        label_50->setFont(font2);
        label_50->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_50->setAlignment(Qt::AlignCenter);
        label_51 = new QLabel(frame_8);
        label_51->setObjectName(QString::fromUtf8("label_51"));
        label_51->setGeometry(QRect(20, 141, 41, 20));
        label_51->setFont(font2);
        label_51->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_51->setAlignment(Qt::AlignCenter);
        label_52 = new QLabel(frame_8);
        label_52->setObjectName(QString::fromUtf8("label_52"));
        label_52->setGeometry(QRect(331, 80, 34, 22));
        label_52->setFont(font2);
        label_52->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_52->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        label_53 = new QLabel(frame_8);
        label_53->setObjectName(QString::fromUtf8("label_53"));
        label_53->setGeometry(QRect(331, 140, 34, 20));
        label_53->setFont(font2);
        label_53->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_53->setAlignment(Qt::AlignCenter);
        frame_3 = new QFrame(setting);
        frame_3->setObjectName(QString::fromUtf8("frame_3"));
        frame_3->setGeometry(QRect(307, 625, 1581, 390));
        frame_3->setStyleSheet(QString::fromUtf8("background-color: rgba(42, 47, 59, 1);"));
        frame_3->setFrameShape(QFrame::StyledPanel);
        frame_3->setFrameShadow(QFrame::Raised);
        label_20 = new QLabel(frame_3);
        label_20->setObjectName(QString::fromUtf8("label_20"));
        label_20->setGeometry(QRect(81, 264, 94, 31));
        label_20->setFont(font1);
        label_20->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_20->setAlignment(Qt::AlignCenter);
        label_21 = new QLabel(frame_3);
        label_21->setObjectName(QString::fromUtf8("label_21"));
        label_21->setGeometry(QRect(69, 106, 118, 128));
        pushButton_2 = new QPushButton(frame_3);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
        pushButton_2->setGeometry(QRect(1125, 270, 408, 50));
        QFont font3;
        font3.setFamily(QString::fromUtf8("HarmonyOS Sans SC Medium"));
        font3.setPointSize(20);
        pushButton_2->setFont(font3);
        pushButton_2->setStyleSheet(QString::fromUtf8("background-color: rgba(25, 31, 39, 1);\n"
"color: rgb(255, 255, 255);"));
        frame_5 = new QFrame(frame_3);
        frame_5->setObjectName(QString::fromUtf8("frame_5"));
        frame_5->setGeometry(QRect(250, 15, 407, 180));
        frame_5->setStyleSheet(QString::fromUtf8("background-color: rgb(61, 67, 84);"));
        frame_5->setFrameShape(QFrame::StyledPanel);
        frame_5->setFrameShadow(QFrame::Raised);
        label_23 = new QLabel(frame_5);
        label_23->setObjectName(QString::fromUtf8("label_23"));
        label_23->setGeometry(QRect(22, 24, 55, 27));
        label_23->setFont(font1);
        label_23->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_23->setAlignment(Qt::AlignCenter);
        lineEdit_18 = new QLineEdit(frame_5);
        lineEdit_18->setObjectName(QString::fromUtf8("lineEdit_18"));
        lineEdit_18->setGeometry(QRect(76, 53, 240, 50));
        lineEdit_18->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 0, 0);\n"
"border-top-color: rgb(0, 0, 0);\n"
"color: rgb(255, 255, 255);\n"
"font: 57 20pt \"HarmonyOS Sans SC Medium\";"));
        lineEdit_18->setAlignment(Qt::AlignCenter);
        lineEdit_19 = new QLineEdit(frame_5);
        lineEdit_19->setObjectName(QString::fromUtf8("lineEdit_19"));
        lineEdit_19->setGeometry(QRect(76, 113, 240, 50));
        lineEdit_19->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 0, 0);\n"
"color: rgb(255, 255, 255);\n"
"font: 57 20pt \"HarmonyOS Sans SC Medium\";"));
        lineEdit_19->setAlignment(Qt::AlignCenter);
        label_54 = new QLabel(frame_5);
        label_54->setObjectName(QString::fromUtf8("label_54"));
        label_54->setGeometry(QRect(20, 70, 41, 20));
        label_54->setFont(font2);
        label_54->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_54->setAlignment(Qt::AlignCenter);
        label_55 = new QLabel(frame_5);
        label_55->setObjectName(QString::fromUtf8("label_55"));
        label_55->setGeometry(QRect(20, 130, 41, 20));
        label_55->setFont(font2);
        label_55->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_55->setAlignment(Qt::AlignCenter);
        label_56 = new QLabel(frame_5);
        label_56->setObjectName(QString::fromUtf8("label_56"));
        label_56->setGeometry(QRect(331, 66, 34, 22));
        label_56->setFont(font2);
        label_56->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_56->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        label_57 = new QLabel(frame_5);
        label_57->setObjectName(QString::fromUtf8("label_57"));
        label_57->setGeometry(QRect(331, 126, 34, 20));
        label_57->setFont(font2);
        label_57->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_57->setAlignment(Qt::AlignCenter);
        frame_6 = new QFrame(frame_3);
        frame_6->setObjectName(QString::fromUtf8("frame_6"));
        frame_6->setGeometry(QRect(680, 15, 407, 180));
        frame_6->setStyleSheet(QString::fromUtf8("background-color: rgb(61, 67, 84);"));
        frame_6->setFrameShape(QFrame::StyledPanel);
        frame_6->setFrameShadow(QFrame::Raised);
        label_24 = new QLabel(frame_6);
        label_24->setObjectName(QString::fromUtf8("label_24"));
        label_24->setGeometry(QRect(22, 24, 55, 27));
        label_24->setFont(font1);
        label_24->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_24->setAlignment(Qt::AlignCenter);
        lineEdit_20 = new QLineEdit(frame_6);
        lineEdit_20->setObjectName(QString::fromUtf8("lineEdit_20"));
        lineEdit_20->setGeometry(QRect(76, 53, 240, 50));
        lineEdit_20->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 0, 0);\n"
"border-top-color: rgb(0, 0, 0);\n"
"color: rgb(255, 255, 255);\n"
"font: 57 20pt \"HarmonyOS Sans SC Medium\";"));
        lineEdit_20->setAlignment(Qt::AlignCenter);
        lineEdit_21 = new QLineEdit(frame_6);
        lineEdit_21->setObjectName(QString::fromUtf8("lineEdit_21"));
        lineEdit_21->setGeometry(QRect(76, 113, 240, 50));
        lineEdit_21->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 0, 0);\n"
"color: rgb(255, 255, 255);\n"
"font: 57 20pt \"HarmonyOS Sans SC Medium\";"));
        lineEdit_21->setAlignment(Qt::AlignCenter);
        label_58 = new QLabel(frame_6);
        label_58->setObjectName(QString::fromUtf8("label_58"));
        label_58->setGeometry(QRect(20, 70, 41, 20));
        label_58->setFont(font2);
        label_58->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_58->setAlignment(Qt::AlignCenter);
        label_59 = new QLabel(frame_6);
        label_59->setObjectName(QString::fromUtf8("label_59"));
        label_59->setGeometry(QRect(20, 130, 41, 20));
        label_59->setFont(font2);
        label_59->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_59->setAlignment(Qt::AlignCenter);
        label_60 = new QLabel(frame_6);
        label_60->setObjectName(QString::fromUtf8("label_60"));
        label_60->setGeometry(QRect(331, 66, 34, 22));
        label_60->setFont(font2);
        label_60->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_60->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        label_61 = new QLabel(frame_6);
        label_61->setObjectName(QString::fromUtf8("label_61"));
        label_61->setGeometry(QRect(331, 126, 34, 20));
        label_61->setFont(font2);
        label_61->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_61->setAlignment(Qt::AlignCenter);
        frame_9 = new QFrame(frame_3);
        frame_9->setObjectName(QString::fromUtf8("frame_9"));
        frame_9->setGeometry(QRect(1140, 15, 407, 180));
        frame_9->setStyleSheet(QString::fromUtf8("background-color: rgb(61, 67, 84);"));
        frame_9->setFrameShape(QFrame::StyledPanel);
        frame_9->setFrameShadow(QFrame::Raised);
        label_62 = new QLabel(frame_9);
        label_62->setObjectName(QString::fromUtf8("label_62"));
        label_62->setGeometry(QRect(22, 23, 55, 30));
        label_62->setFont(font1);
        label_62->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_62->setAlignment(Qt::AlignCenter);
        lineEdit_22 = new QLineEdit(frame_9);
        lineEdit_22->setObjectName(QString::fromUtf8("lineEdit_22"));
        lineEdit_22->setGeometry(QRect(76, 53, 240, 50));
        lineEdit_22->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 0, 0);\n"
"border-top-color: rgb(0, 0, 0);\n"
"color: rgb(255, 255, 255);\n"
"font: 57 20pt \"HarmonyOS Sans SC Medium\";"));
        lineEdit_22->setAlignment(Qt::AlignCenter);
        lineEdit_23 = new QLineEdit(frame_9);
        lineEdit_23->setObjectName(QString::fromUtf8("lineEdit_23"));
        lineEdit_23->setGeometry(QRect(76, 113, 240, 50));
        lineEdit_23->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 0, 0);\n"
"color: rgb(255, 255, 255);\n"
"font: 57 20pt \"HarmonyOS Sans SC Medium\";"));
        lineEdit_23->setAlignment(Qt::AlignCenter);
        label_63 = new QLabel(frame_9);
        label_63->setObjectName(QString::fromUtf8("label_63"));
        label_63->setGeometry(QRect(20, 70, 41, 20));
        label_63->setFont(font2);
        label_63->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_63->setAlignment(Qt::AlignCenter);
        label_64 = new QLabel(frame_9);
        label_64->setObjectName(QString::fromUtf8("label_64"));
        label_64->setGeometry(QRect(20, 130, 41, 20));
        label_64->setFont(font2);
        label_64->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_64->setAlignment(Qt::AlignCenter);
        label_65 = new QLabel(frame_9);
        label_65->setObjectName(QString::fromUtf8("label_65"));
        label_65->setGeometry(QRect(331, 66, 34, 22));
        label_65->setFont(font2);
        label_65->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_65->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        label_66 = new QLabel(frame_9);
        label_66->setObjectName(QString::fromUtf8("label_66"));
        label_66->setGeometry(QRect(331, 126, 34, 20));
        label_66->setFont(font2);
        label_66->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_66->setAlignment(Qt::AlignCenter);
        frame_10 = new QFrame(frame_3);
        frame_10->setObjectName(QString::fromUtf8("frame_10"));
        frame_10->setGeometry(QRect(250, 205, 407, 180));
        frame_10->setStyleSheet(QString::fromUtf8("background-color: rgb(61, 67, 84);"));
        frame_10->setFrameShape(QFrame::StyledPanel);
        frame_10->setFrameShadow(QFrame::Raised);
        label_67 = new QLabel(frame_10);
        label_67->setObjectName(QString::fromUtf8("label_67"));
        label_67->setGeometry(QRect(22, 24, 55, 27));
        label_67->setFont(font1);
        label_67->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_67->setAlignment(Qt::AlignCenter);
        lineEdit_24 = new QLineEdit(frame_10);
        lineEdit_24->setObjectName(QString::fromUtf8("lineEdit_24"));
        lineEdit_24->setGeometry(QRect(76, 53, 240, 50));
        lineEdit_24->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 0, 0);\n"
"border-top-color: rgb(0, 0, 0);\n"
"color: rgb(255, 255, 255);\n"
"font: 57 20pt \"HarmonyOS Sans SC Medium\";"));
        lineEdit_24->setAlignment(Qt::AlignCenter);
        lineEdit_25 = new QLineEdit(frame_10);
        lineEdit_25->setObjectName(QString::fromUtf8("lineEdit_25"));
        lineEdit_25->setGeometry(QRect(76, 113, 240, 50));
        lineEdit_25->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 0, 0);\n"
"color: rgb(255, 255, 255);\n"
"font: 57 20pt \"HarmonyOS Sans SC Medium\";"));
        lineEdit_25->setAlignment(Qt::AlignCenter);
        label_68 = new QLabel(frame_10);
        label_68->setObjectName(QString::fromUtf8("label_68"));
        label_68->setGeometry(QRect(20, 70, 41, 20));
        label_68->setFont(font2);
        label_68->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_68->setAlignment(Qt::AlignCenter);
        label_69 = new QLabel(frame_10);
        label_69->setObjectName(QString::fromUtf8("label_69"));
        label_69->setGeometry(QRect(20, 130, 41, 20));
        label_69->setFont(font2);
        label_69->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_69->setAlignment(Qt::AlignCenter);
        label_70 = new QLabel(frame_10);
        label_70->setObjectName(QString::fromUtf8("label_70"));
        label_70->setGeometry(QRect(331, 66, 34, 22));
        label_70->setFont(font2);
        label_70->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_70->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        label_71 = new QLabel(frame_10);
        label_71->setObjectName(QString::fromUtf8("label_71"));
        label_71->setGeometry(QRect(331, 126, 34, 20));
        label_71->setFont(font2);
        label_71->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_71->setAlignment(Qt::AlignCenter);
        frame_11 = new QFrame(frame_3);
        frame_11->setObjectName(QString::fromUtf8("frame_11"));
        frame_11->setGeometry(QRect(680, 205, 407, 180));
        frame_11->setStyleSheet(QString::fromUtf8("background-color: rgb(61, 67, 84);"));
        frame_11->setFrameShape(QFrame::StyledPanel);
        frame_11->setFrameShadow(QFrame::Raised);
        label_72 = new QLabel(frame_11);
        label_72->setObjectName(QString::fromUtf8("label_72"));
        label_72->setGeometry(QRect(22, 24, 55, 27));
        label_72->setFont(font1);
        label_72->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_72->setAlignment(Qt::AlignCenter);
        lineEdit_26 = new QLineEdit(frame_11);
        lineEdit_26->setObjectName(QString::fromUtf8("lineEdit_26"));
        lineEdit_26->setGeometry(QRect(76, 53, 240, 50));
        lineEdit_26->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 0, 0);\n"
"border-top-color: rgb(0, 0, 0);\n"
"color: rgb(255, 255, 255);\n"
"font: 57 20pt \"HarmonyOS Sans SC Medium\";"));
        lineEdit_26->setAlignment(Qt::AlignCenter);
        lineEdit_27 = new QLineEdit(frame_11);
        lineEdit_27->setObjectName(QString::fromUtf8("lineEdit_27"));
        lineEdit_27->setGeometry(QRect(76, 113, 240, 50));
        lineEdit_27->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 0, 0);\n"
"color: rgb(255, 255, 255);\n"
"font: 57 20pt \"HarmonyOS Sans SC Medium\";"));
        lineEdit_27->setAlignment(Qt::AlignCenter);
        label_73 = new QLabel(frame_11);
        label_73->setObjectName(QString::fromUtf8("label_73"));
        label_73->setGeometry(QRect(20, 70, 41, 20));
        label_73->setFont(font2);
        label_73->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_73->setAlignment(Qt::AlignCenter);
        label_74 = new QLabel(frame_11);
        label_74->setObjectName(QString::fromUtf8("label_74"));
        label_74->setGeometry(QRect(20, 130, 41, 20));
        label_74->setFont(font2);
        label_74->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_74->setAlignment(Qt::AlignCenter);
        label_75 = new QLabel(frame_11);
        label_75->setObjectName(QString::fromUtf8("label_75"));
        label_75->setGeometry(QRect(331, 66, 34, 22));
        label_75->setFont(font2);
        label_75->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_75->setAlignment(Qt::AlignHCenter|Qt::AlignTop);
        label_76 = new QLabel(frame_11);
        label_76->setObjectName(QString::fromUtf8("label_76"));
        label_76->setGeometry(QRect(331, 126, 34, 20));
        label_76->setFont(font2);
        label_76->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_76->setAlignment(Qt::AlignCenter);
        frame_12 = new QFrame(setting);
        frame_12->setObjectName(QString::fromUtf8("frame_12"));
        frame_12->setGeometry(QRect(40, 90, 469, 316));
        frame_12->setStyleSheet(QString::fromUtf8("background-color: rgba(42, 47, 59, 1);"));
        frame_12->setFrameShape(QFrame::StyledPanel);
        frame_12->setFrameShadow(QFrame::Raised);
        label_5 = new QLabel(frame_12);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(174, 244, 121, 30));
        label_5->setFont(font1);
        label_5->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_5->setAlignment(Qt::AlignCenter);
        label_4 = new QLabel(frame_12);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(139, 27, 192, 192));
        label_4->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        frame_13 = new QFrame(setting);
        frame_13->setObjectName(QString::fromUtf8("frame_13"));
        frame_13->setGeometry(QRect(520, 90, 1362, 316));
        frame_13->setStyleSheet(QString::fromUtf8("background-color: rgba(42, 47, 59, 1);"));
        frame_13->setFrameShape(QFrame::StyledPanel);
        frame_13->setFrameShadow(QFrame::Raised);
        label_8 = new QLabel(frame_13);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setGeometry(QRect(21, 209, 78, 20));
        QFont font4;
        font4.setFamily(QString::fromUtf8("HarmonyOS Sans SC Medium"));
        font4.setPointSize(14);
        label_8->setFont(font4);
        label_8->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_8->setAlignment(Qt::AlignCenter);
        label_11 = new QLabel(frame_13);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        label_11->setGeometry(QRect(1283, 86, 20, 19));
        label_11->setFont(font2);
        label_11->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_11->setAlignment(Qt::AlignCenter);
        lineEdit_5 = new QLineEdit(frame_13);
        lineEdit_5->setObjectName(QString::fromUtf8("lineEdit_5"));
        lineEdit_5->setGeometry(QRect(821, 130, 448, 50));
        lineEdit_5->setFont(font3);
        lineEdit_5->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);\n"
"background-color: rgb(0, 0, 0);"));
        lineEdit_5->setAlignment(Qt::AlignCenter);
        label_7 = new QLabel(frame_13);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setGeometry(QRect(21, 146, 97, 19));
        label_7->setFont(font4);
        label_7->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_7->setAlignment(Qt::AlignCenter);
        label_13 = new QLabel(frame_13);
        label_13->setObjectName(QString::fromUtf8("label_13"));
        label_13->setGeometry(QRect(1284, 148, 20, 19));
        label_13->setFont(font2);
        label_13->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_13->setAlignment(Qt::AlignCenter);
        label_15 = new QLabel(frame_13);
        label_15->setObjectName(QString::fromUtf8("label_15"));
        label_15->setGeometry(QRect(959, 259, 20, 19));
        label_15->setFont(font2);
        label_15->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_15->setAlignment(Qt::AlignCenter);
        label_10 = new QLabel(frame_13);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setGeometry(QRect(624, 148, 38, 16));
        QFont font5;
        font5.setFamily(QString::fromUtf8("HarmonyOS Sans SC Medium"));
        font5.setPointSize(9);
        label_10->setFont(font5);
        label_10->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_10->setAlignment(Qt::AlignCenter);
        lineEdit_3 = new QLineEdit(frame_13);
        lineEdit_3->setObjectName(QString::fromUtf8("lineEdit_3"));
        lineEdit_3->setGeometry(QRect(20, 241, 924, 50));
        lineEdit_3->setFont(font3);
        lineEdit_3->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);\n"
"background-color: rgb(0, 0, 0);"));
        lineEdit_3->setAlignment(Qt::AlignCenter);
        label_14 = new QLabel(frame_13);
        label_14->setObjectName(QString::fromUtf8("label_14"));
        label_14->setGeometry(QRect(681, 148, 124, 21));
        label_14->setFont(font4);
        label_14->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_14->setAlignment(Qt::AlignCenter);
        lineEdit_4 = new QLineEdit(frame_13);
        lineEdit_4->setObjectName(QString::fromUtf8("lineEdit_4"));
        lineEdit_4->setGeometry(QRect(821, 70, 448, 50));
        lineEdit_4->setFont(font3);
        lineEdit_4->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);\n"
"background-color: rgb(0, 0, 0);"));
        lineEdit_4->setAlignment(Qt::AlignCenter);
        label_6 = new QLabel(frame_13);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(21, 86, 124, 21));
        label_6->setFont(font4);
        label_6->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_6->setAlignment(Qt::AlignCenter);
        label_12 = new QLabel(frame_13);
        label_12->setObjectName(QString::fromUtf8("label_12"));
        label_12->setGeometry(QRect(681, 86, 124, 21));
        label_12->setFont(font4);
        label_12->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_12->setAlignment(Qt::AlignCenter);
        lineEdit_2 = new QLineEdit(frame_13);
        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));
        lineEdit_2->setGeometry(QRect(160, 130, 448, 50));
        lineEdit_2->setFont(font3);
        lineEdit_2->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);\n"
"background-color: rgb(0, 0, 0);"));
        lineEdit_2->setAlignment(Qt::AlignCenter);
        label = new QLabel(frame_13);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(21, 23, 109, 27));
        label->setFont(font3);
        label->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_9 = new QLabel(frame_13);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setGeometry(QRect(623, 86, 20, 19));
        label_9->setFont(font2);
        label_9->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_9->setAlignment(Qt::AlignCenter);
        lineEdit = new QLineEdit(frame_13);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        lineEdit->setGeometry(QRect(160, 70, 448, 50));
        lineEdit->setFont(font3);
        lineEdit->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);\n"
"border-right-color: rgb(0, 0, 0);\n"
"border-top-color: rgb(0, 0, 0);\n"
"background-color: rgb(0, 0, 0);\n"
"border-bottom-color: rgb(0, 0, 0);\n"
"border-left-color: rgb(0, 0, 0);"));
        lineEdit->setAlignment(Qt::AlignCenter);

        retranslateUi(setting);

        QMetaObject::connectSlotsByName(setting);
    } // setupUi

    void retranslateUi(QWidget *setting)
    {
        setting->setWindowTitle(QApplication::translate("setting", "Form", nullptr));
        pushButton->setText(QApplication::translate("setting", "PushButton", nullptr));
        label_2->setText(QApplication::translate("setting", "<html><head/><body><p><img src=\":/image1/7.png\"/></p></body></html>", nullptr));
        label_3->setText(QApplication::translate("setting", "TextLabel", nullptr));
        label_16->setText(QApplication::translate("setting", "\346\212\245\350\255\246\350\256\276\347\275\256", nullptr));
        label_17->setText(QApplication::translate("setting", "<html><head/><body><p><img src=\":/image1/Alarm settings.png\"/></p></body></html>", nullptr));
        label_18->setText(QApplication::translate("setting", "<html><head/><body><p><img src=\":/image1/motor.png\"/></p></body></html>", nullptr));
        label_19->setText(QApplication::translate("setting", "\347\224\265\346\234\272", nullptr));
        label_22->setText(QApplication::translate("setting", "\350\275\254\351\200\237", nullptr));
        label_25->setText(QApplication::translate("setting", "Max", nullptr));
        label_26->setText(QApplication::translate("setting", "Min", nullptr));
        label_27->setText(QApplication::translate("setting", "rpm", nullptr));
        label_28->setText(QApplication::translate("setting", "rpm", nullptr));
        label_39->setText(QApplication::translate("setting", "\347\236\254\346\227\266\346\265\201\351\207\217", nullptr));
        label_40->setText(QApplication::translate("setting", "Max", nullptr));
        label_41->setText(QApplication::translate("setting", "Min", nullptr));
        label_42->setText(QApplication::translate("setting", "L/min", nullptr));
        label_43->setText(QApplication::translate("setting", "L/min", nullptr));
        label_49->setText(QApplication::translate("setting", "\347\224\265\346\265\201", nullptr));
        label_50->setText(QApplication::translate("setting", "Max", nullptr));
        label_51->setText(QApplication::translate("setting", "Min", nullptr));
        label_52->setText(QApplication::translate("setting", "A", nullptr));
        label_53->setText(QApplication::translate("setting", "A", nullptr));
        label_20->setText(QApplication::translate("setting", "\344\274\240\346\204\237\345\231\250", nullptr));
        label_21->setText(QApplication::translate("setting", "<html><head/><body><p><img src=\":/image1/2.png\"/></p></body></html>", nullptr));
        pushButton_2->setText(QApplication::translate("setting", "\350\256\276\347\275\256", nullptr));
        label_23->setText(QApplication::translate("setting", "HCT", nullptr));
        label_54->setText(QApplication::translate("setting", "Max", nullptr));
        label_55->setText(QApplication::translate("setting", "Min", nullptr));
        label_56->setText(QApplication::translate("setting", "%", nullptr));
        label_57->setText(QApplication::translate("setting", "%", nullptr));
        label_24->setText(QApplication::translate("setting", "PO2", nullptr));
        label_58->setText(QApplication::translate("setting", "Max", nullptr));
        label_59->setText(QApplication::translate("setting", "Min", nullptr));
        label_60->setText(QApplication::translate("setting", "KPa", nullptr));
        label_61->setText(QApplication::translate("setting", "KPa", nullptr));
        label_62->setText(QApplication::translate("setting", "Hgb", nullptr));
        label_63->setText(QApplication::translate("setting", "Max", nullptr));
        label_64->setText(QApplication::translate("setting", "Min", nullptr));
        label_65->setText(QApplication::translate("setting", "g/dL", nullptr));
        label_66->setText(QApplication::translate("setting", "g/dL", nullptr));
        label_67->setText(QApplication::translate("setting", "SO2", nullptr));
        label_68->setText(QApplication::translate("setting", "Max", nullptr));
        label_69->setText(QApplication::translate("setting", "Min", nullptr));
        label_70->setText(QApplication::translate("setting", "%", nullptr));
        label_71->setText(QApplication::translate("setting", "%", nullptr));
        label_72->setText(QApplication::translate("setting", "T", nullptr));
        label_73->setText(QApplication::translate("setting", "Max", nullptr));
        label_74->setText(QApplication::translate("setting", "Min", nullptr));
        label_75->setText(QApplication::translate("setting", "\342\204\203", nullptr));
        label_76->setText(QApplication::translate("setting", "\342\204\203", nullptr));
        label_5->setText(QApplication::translate("setting", "\347\224\265\346\234\272\346\216\247\345\210\266", nullptr));
        label_4->setText(QApplication::translate("setting", "<html><head/><body><p><img src=\":/image1/1.png\"/></p></body></html>", nullptr));
        label_8->setText(QApplication::translate("setting", "\351\242\235\345\256\232\347\224\265\346\265\201", nullptr));
        label_11->setText(QApplication::translate("setting", "\347\247\222", nullptr));
        lineEdit_5->setText(QString());
        label_7->setText(QApplication::translate("setting", "\345\212\240\351\200\237\345\212\240\351\200\237\345\272\246", nullptr));
        label_13->setText(QApplication::translate("setting", "\347\247\222", nullptr));
        label_15->setText(QApplication::translate("setting", "A", nullptr));
        label_10->setText(QApplication::translate("setting", "HZ/S", nullptr));
        lineEdit_3->setText(QString());
        label_14->setText(QApplication::translate("setting", "\345\207\217\351\200\237\345\212\240\351\200\237\345\272\246", nullptr));
        lineEdit_4->setText(QString());
        label_6->setText(QApplication::translate("setting", "pwm\344\270\212\345\215\207\347\274\223\345\206\262", nullptr));
        label_12->setText(QApplication::translate("setting", "pwm\344\270\213\351\231\215\347\274\223\345\206\262", nullptr));
        lineEdit_2->setText(QString());
        label->setText(QApplication::translate("setting", "\346\265\201\351\207\217\351\207\207\351\233\206", nullptr));
        label_9->setText(QApplication::translate("setting", "\347\247\222", nullptr));
        lineEdit->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class setting: public Ui_setting {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SETTING_H
