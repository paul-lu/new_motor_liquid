/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QLabel *label;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;
    QPushButton *pushButton_4;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QGroupBox *groupBox;
    QLabel *label_5;
    QLabel *label_20;
    QLabel *label_25;
    QLabel *label_26;
    QLabel *label_27;
    QLabel *label_28;
    QLabel *label_7;
    QLabel *label_29;
    QLabel *label_8;
    QLabel *label_10;
    QLabel *label_30;
    QLabel *label_31;
    QLabel *label_32;
    QGroupBox *groupBox_2;
    QLabel *label_9;
    QLabel *label_14;
    QLabel *label_15;
    QLabel *label_16;
    QLabel *label_17;
    QPushButton *pushButton_5;
    QPushButton *pushButton_6;
    QPushButton *pushButton_7;
    QPushButton *pushButton_8;
    QPushButton *pushButton_9;
    QPushButton *pushButton_10;
    QLabel *label_18;
    QLabel *label_19;
    QSlider *horizontalSlider;
    QLabel *label_21;
    QLabel *label_22;
    QLabel *label_23;
    QLabel *label_24;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(1920, 1080);
        MainWindow->setStyleSheet(QString::fromUtf8("background-color: rgba(0, 7, 15, 1);"));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        label = new QLabel(centralwidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(40, 34, 55, 31));
        pushButton = new QPushButton(centralwidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(950, 19, 194, 58));
        pushButton->setStyleSheet(QString::fromUtf8("background-color: rgba(25, 31, 39, 1);\n"
"font: 57 20pt \"HarmonyOS Sans SC Medium\";\n"
"color: rgb(255, 255, 255);"));
        pushButton_2 = new QPushButton(centralwidget);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
        pushButton_2->setGeometry(QRect(1403, 21, 190, 58));
        pushButton_2->setStyleSheet(QString::fromUtf8("background-color: rgba(2, 199, 205, 1);\n"
"font: 57 20pt \"HarmonyOS Sans SC Medium\";"));
        pushButton_3 = new QPushButton(centralwidget);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));
        pushButton_3->setGeometry(QRect(1798, 21, 82, 58));
        pushButton_3->setStyleSheet(QString::fromUtf8("background-color: rgba(25, 31, 39, 1);\n"
"color: rgb(255, 255, 255);\n"
"font: 57 20pt \"HarmonyOS Sans SC Medium\";"));
        pushButton_4 = new QPushButton(centralwidget);
        pushButton_4->setObjectName(QString::fromUtf8("pushButton_4"));
        pushButton_4->setGeometry(QRect(1593, 21, 190, 58));
        pushButton_4->setStyleSheet(QString::fromUtf8("background-color: rgba(25, 31, 39, 1);\n"
"font: 57 20pt \"HarmonyOS Sans SC Medium\";\n"
"color: rgb(255, 255, 255);"));
        label_2 = new QLabel(centralwidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(120, 30, 443, 36));
        QFont font;
        font.setFamily(QString::fromUtf8("HarmonyOS Sans SC Medium"));
        font.setPointSize(22);
        label_2->setFont(font);
        label_2->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_2->setAlignment(Qt::AlignCenter);
        label_3 = new QLabel(centralwidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(1184, 30, 50, 41));
        label_3->setStyleSheet(QString::fromUtf8(""));
        label_3->setPixmap(QPixmap(QString::fromUtf8(":/image1/iwarn2.png")));
        label_4 = new QLabel(centralwidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(1254, 38, 119, 29));
        QFont font1;
        font1.setFamily(QString::fromUtf8("HarmonyOS Sans SC Medium"));
        font1.setPointSize(20);
        label_4->setFont(font1);
        label_4->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_4->setAlignment(Qt::AlignCenter);
        groupBox = new QGroupBox(centralwidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setGeometry(QRect(40, 90, 1840, 525));
        groupBox->setStyleSheet(QString::fromUtf8("background-color: rgba(42, 47, 59, 1);"));
        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(656, 31, 190, 58));
        QFont font2;
        font2.setFamily(QString::fromUtf8("HarmonyOS Sans SC Medium"));
        font2.setPointSize(24);
        label_5->setFont(font2);
        label_5->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 0, 0);\n"
"color: rgb(255, 255, 255);"));
        label_5->setAlignment(Qt::AlignCenter);
        label_20 = new QLabel(groupBox);
        label_20->setObjectName(QString::fromUtf8("label_20"));
        label_20->setGeometry(QRect(990, 31, 190, 58));
        label_20->setFont(font2);
        label_20->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 0, 0);\n"
"color: rgb(255, 255, 255);"));
        label_20->setAlignment(Qt::AlignCenter);
        label_25 = new QLabel(groupBox);
        label_25->setObjectName(QString::fromUtf8("label_25"));
        label_25->setGeometry(QRect(80, 250, 81, 30));
        label_25->setFont(font2);
        label_25->setStyleSheet(QString::fromUtf8("color: rgb(79, 255, 255);"));
        label_25->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        label_26 = new QLabel(groupBox);
        label_26->setObjectName(QString::fromUtf8("label_26"));
        label_26->setGeometry(QRect(1670, 190, 71, 41));
        label_26->setFont(font2);
        label_26->setStyleSheet(QString::fromUtf8("color: rgb(79, 255, 255);"));
        label_26->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);
        label_27 = new QLabel(groupBox);
        label_27->setObjectName(QString::fromUtf8("label_27"));
        label_27->setGeometry(QRect(431, 440, 190, 58));
        label_27->setFont(font1);
        label_27->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);\n"
"background-color: rgb(0, 0, 0);\n"
""));
        label_27->setAlignment(Qt::AlignCenter);
        label_28 = new QLabel(groupBox);
        label_28->setObjectName(QString::fromUtf8("label_28"));
        label_28->setGeometry(QRect(1060, 440, 238, 58));
        label_28->setFont(font1);
        label_28->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);\n"
"background-color: rgb(0, 0, 0);\n"
""));
        label_28->setAlignment(Qt::AlignCenter);
        label_7 = new QLabel(groupBox);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setGeometry(QRect(170, 100, 1501, 341));
        label_29 = new QLabel(groupBox);
        label_29->setObjectName(QString::fromUtf8("label_29"));
        label_29->setGeometry(QRect(1110, 280, 238, 58));
        label_29->setFont(font1);
        label_29->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);\n"
"background-color: rgb(0, 0, 0);\n"
""));
        label_29->setAlignment(Qt::AlignCenter);
        label_8 = new QLabel(groupBox);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setGeometry(QRect(280, 270, 278, 58));
        label_8->setFont(font1);
        label_8->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);\n"
"background-color: rgb(0, 0, 0);\n"
""));
        label_8->setAlignment(Qt::AlignCenter);
        label_10 = new QLabel(groupBox);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setGeometry(QRect(555, 270, 278, 58));
        label_10->setFont(font1);
        label_10->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);\n"
"background-color: rgb(0, 0, 0);\n"
""));
        label_10->setAlignment(Qt::AlignCenter);
        label_30 = new QLabel(groupBox);
        label_30->setObjectName(QString::fromUtf8("label_30"));
        label_30->setGeometry(QRect(1345, 280, 238, 58));
        label_30->setFont(font1);
        label_30->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);\n"
"background-color: rgb(0, 0, 0);\n"
""));
        label_30->setAlignment(Qt::AlignCenter);
        label_31 = new QLabel(groupBox);
        label_31->setObjectName(QString::fromUtf8("label_31"));
        label_31->setGeometry(QRect(1295, 440, 238, 58));
        label_31->setFont(font1);
        label_31->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);\n"
"background-color: rgb(0, 0, 0);\n"
""));
        label_31->setAlignment(Qt::AlignCenter);
        label_32 = new QLabel(groupBox);
        label_32->setObjectName(QString::fromUtf8("label_32"));
        label_32->setGeometry(QRect(800, 200, 234, 58));
        label_32->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);\n"
"background-color: rgb(0, 0, 0);\n"
"font: 57 20pt \"HarmonyOS Sans SC Medium\";"));
        label_32->setAlignment(Qt::AlignCenter);
        groupBox_2 = new QGroupBox(centralwidget);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setGeometry(QRect(40, 625, 1840, 380));
        groupBox_2->setStyleSheet(QString::fromUtf8("background-color: rgba(42, 47, 59, 1);\n"
""));
        label_9 = new QLabel(groupBox_2);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setGeometry(QRect(60, 39, 125, 31));
        label_9->setStyleSheet(QString::fromUtf8("\n"
"font: 57 20pt \"HarmonyOS Sans SC Medium\";\n"
"color: rgb(255, 255, 255);"));
        label_9->setAlignment(Qt::AlignCenter);
        label_14 = new QLabel(groupBox_2);
        label_14->setObjectName(QString::fromUtf8("label_14"));
        label_14->setGeometry(QRect(60, 100, 77, 20));
        QFont font3;
        font3.setFamily(QString::fromUtf8("HarmonyOS Sans SC Medium"));
        font3.setPointSize(14);
        label_14->setFont(font3);
        label_14->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_14->setAlignment(Qt::AlignCenter);
        label_15 = new QLabel(groupBox_2);
        label_15->setObjectName(QString::fromUtf8("label_15"));
        label_15->setGeometry(QRect(60, 132, 330, 58));
        label_15->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);\n"
"background-color: rgb(0, 0, 0);\n"
"font: 57 20pt \"HarmonyOS Sans SC Medium\";"));
        label_16 = new QLabel(groupBox_2);
        label_16->setObjectName(QString::fromUtf8("label_16"));
        label_16->setGeometry(QRect(645, 132, 330, 58));
        label_16->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);\n"
"background-color: rgb(0, 0, 0);\n"
"font: 57 20pt \"HarmonyOS Sans SC Medium\";"));
        label_17 = new QLabel(groupBox_2);
        label_17->setObjectName(QString::fromUtf8("label_17"));
        label_17->setGeometry(QRect(1230, 132, 330, 58));
        label_17->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);\n"
"background-color: rgb(0, 0, 0);\n"
"font: 57 20pt \"HarmonyOS Sans SC Medium\";"));
        pushButton_5 = new QPushButton(groupBox_2);
        pushButton_5->setObjectName(QString::fromUtf8("pushButton_5"));
        pushButton_5->setGeometry(QRect(400, 132, 100, 58));
        pushButton_5->setFont(font3);
        pushButton_5->setStyleSheet(QString::fromUtf8("background-color: rgba(25, 31, 39, 1);\n"
"color: rgb(255, 255, 255);"));
        pushButton_6 = new QPushButton(groupBox_2);
        pushButton_6->setObjectName(QString::fromUtf8("pushButton_6"));
        pushButton_6->setGeometry(QRect(510, 132, 100, 58));
        pushButton_6->setFont(font3);
        pushButton_6->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);\n"
"background-color: rgba(25, 31, 39, 1);"));
        pushButton_7 = new QPushButton(groupBox_2);
        pushButton_7->setObjectName(QString::fromUtf8("pushButton_7"));
        pushButton_7->setGeometry(QRect(985, 132, 100, 58));
        pushButton_7->setFont(font3);
        pushButton_7->setStyleSheet(QString::fromUtf8("background-color: rgba(25, 31, 39, 1);\n"
"color: rgb(255, 255, 255);"));
        pushButton_8 = new QPushButton(groupBox_2);
        pushButton_8->setObjectName(QString::fromUtf8("pushButton_8"));
        pushButton_8->setGeometry(QRect(1095, 132, 100, 58));
        pushButton_8->setFont(font3);
        pushButton_8->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);\n"
"background-color: rgba(25, 31, 39, 1);"));
        pushButton_9 = new QPushButton(groupBox_2);
        pushButton_9->setObjectName(QString::fromUtf8("pushButton_9"));
        pushButton_9->setGeometry(QRect(1570, 132, 100, 58));
        pushButton_9->setFont(font3);
        pushButton_9->setStyleSheet(QString::fromUtf8("background-color: rgba(25, 31, 39, 1);\n"
"color: rgb(255, 255, 255);"));
        pushButton_10 = new QPushButton(groupBox_2);
        pushButton_10->setObjectName(QString::fromUtf8("pushButton_10"));
        pushButton_10->setGeometry(QRect(1680, 132, 100, 58));
        pushButton_10->setFont(font3);
        pushButton_10->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);\n"
"background-color: rgba(25, 31, 39, 1);"));
        label_18 = new QLabel(groupBox_2);
        label_18->setObjectName(QString::fromUtf8("label_18"));
        label_18->setGeometry(QRect(645, 100, 77, 20));
        label_18->setFont(font3);
        label_18->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_18->setAlignment(Qt::AlignCenter);
        label_19 = new QLabel(groupBox_2);
        label_19->setObjectName(QString::fromUtf8("label_19"));
        label_19->setGeometry(QRect(1230, 100, 100, 20));
        label_19->setFont(font3);
        label_19->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 255);"));
        label_19->setAlignment(Qt::AlignCenter);
        horizontalSlider = new QSlider(groupBox_2);
        horizontalSlider->setObjectName(QString::fromUtf8("horizontalSlider"));
        horizontalSlider->setGeometry(QRect(60, 280, 1721, 35));
        QFont font4;
        font4.setFamily(QString::fromUtf8("Arial"));
        font4.setPointSize(16);
        horizontalSlider->setFont(font4);
        horizontalSlider->setStyleSheet(QString::fromUtf8("alternate-background-color: rgb(85, 255, 255);\n"
"border-color: rgb(85, 170, 255);\n"
"color: rgb(255, 85, 255);\n"
"\n"
""));
        horizontalSlider->setMaximum(5000);
        horizontalSlider->setValue(100);
        horizontalSlider->setOrientation(Qt::Horizontal);
        horizontalSlider->setTickInterval(0);
        label_21 = new QLabel(groupBox_2);
        label_21->setObjectName(QString::fromUtf8("label_21"));
        label_21->setGeometry(QRect(57, 240, 125, 31));
        label_21->setStyleSheet(QString::fromUtf8("\n"
"font: 57 20pt \"HarmonyOS Sans SC Medium\";\n"
"color: rgb(255, 255, 255);"));
        label_21->setAlignment(Qt::AlignCenter);
        label_22 = new QLabel(groupBox_2);
        label_22->setObjectName(QString::fromUtf8("label_22"));
        label_22->setGeometry(QRect(57, 330, 81, 32));
        QFont font5;
        font5.setFamily(QString::fromUtf8("HarmonyOS Sans SC Medium"));
        font5.setPointSize(16);
        font5.setBold(false);
        font5.setItalic(false);
        font5.setWeight(50);
        label_22->setFont(font5);
        label_22->setStyleSheet(QString::fromUtf8("\n"
"color: rgb(255, 255, 255);"));
        label_22->setAlignment(Qt::AlignCenter);
        label_23 = new QLabel(groupBox_2);
        label_23->setObjectName(QString::fromUtf8("label_23"));
        label_23->setGeometry(QRect(1646, 330, 134, 32));
        label_23->setFont(font5);
        label_23->setStyleSheet(QString::fromUtf8("\n"
"color: rgb(255, 255, 255);"));
        label_23->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        label_24 = new QLabel(groupBox_2);
        label_24->setObjectName(QString::fromUtf8("label_24"));
        label_24->setGeometry(QRect(850, 330, 134, 32));
        label_24->setFont(font5);
        label_24->setStyleSheet(QString::fromUtf8("color: rgb(7, 234, 244);"));
        label_24->setAlignment(Qt::AlignCenter);
        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 1920, 23));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", nullptr));
        label->setText(QApplication::translate("MainWindow", "<html><head/><body><p><img src=\":/image1/7.png\"/></p></body></html>", nullptr));
        pushButton->setText(QApplication::translate("MainWindow", "\344\270\262\345\217\243\351\205\215\347\275\256", nullptr));
        pushButton_2->setText(QApplication::translate("MainWindow", "\345\244\215\344\275\215", nullptr));
        pushButton_3->setText(QApplication::translate("MainWindow", "\350\256\276\347\275\256", nullptr));
        pushButton_4->setText(QApplication::translate("MainWindow", "\351\235\231\351\237\263", nullptr));
        label_2->setText(QApplication::translate("MainWindow", "TextLabel", nullptr));
        label_3->setText(QString());
        label_4->setText(QApplication::translate("MainWindow", "\346\255\243\345\270\270\345\267\245\344\275\234", nullptr));
        groupBox->setTitle(QString());
        label_5->setText(QApplication::translate("MainWindow", "AIR", nullptr));
        label_20->setText(QApplication::translate("MainWindow", "O2", nullptr));
        label_25->setText(QApplication::translate("MainWindow", "36\342\204\203", nullptr));
        label_26->setText(QApplication::translate("MainWindow", "36\342\204\203", nullptr));
        label_27->setText(QApplication::translate("MainWindow", "<html><head/><body><p>\347\224\265\346\265\201</p></body></html>", nullptr));
        label_28->setText(QApplication::translate("MainWindow", "<html><head/><body><p>HCT:</p></body></html>", nullptr));
        label_7->setText(QApplication::translate("MainWindow", "<html><head/><body><p><img src=\":/image1/8.png\"/></p></body></html>", nullptr));
        label_29->setText(QApplication::translate("MainWindow", "<html><head/><body><p>PO2:</p></body></html>", nullptr));
        label_8->setText(QApplication::translate("MainWindow", "<html><head/><body><p>\350\275\254\351\200\237\357\274\232</p></body></html>", nullptr));
        label_10->setText(QApplication::translate("MainWindow", "<html><head/><body><p>\346\265\201\351\207\217\357\274\232</p></body></html>", nullptr));
        label_30->setText(QApplication::translate("MainWindow", "<html><head/><body><p>SO2:</p></body></html>", nullptr));
        label_31->setText(QApplication::translate("MainWindow", "<html><head/><body><p>Hgb:</p></body></html>", nullptr));
        label_32->setText(QApplication::translate("MainWindow", "<html><head/><body><p align=\"center\">\346\216\247\346\270\251\344\273\252</p></body></html>", nullptr));
        groupBox_2->setTitle(QString());
        label_9->setText(QApplication::translate("MainWindow", "\346\265\201\351\207\217\351\207\207\351\233\206", nullptr));
        label_14->setText(QApplication::translate("MainWindow", "\347\236\254\346\227\266\346\265\201\351\207\217", nullptr));
        label_15->setText(QString());
        label_16->setText(QString());
        label_17->setText(QString());
        pushButton_5->setText(QApplication::translate("MainWindow", "\351\207\207\351\233\206", nullptr));
        pushButton_6->setText(QApplication::translate("MainWindow", "\345\201\234\346\255\242", nullptr));
        pushButton_7->setText(QApplication::translate("MainWindow", "\351\207\207\351\233\206", nullptr));
        pushButton_8->setText(QApplication::translate("MainWindow", "\345\201\234\346\255\242", nullptr));
        pushButton_9->setText(QApplication::translate("MainWindow", "\351\207\207\351\233\206", nullptr));
        pushButton_10->setText(QApplication::translate("MainWindow", "\345\201\234\346\255\242", nullptr));
        label_18->setText(QApplication::translate("MainWindow", "\347\236\254\346\227\266\346\265\201\351\200\237", nullptr));
        label_19->setText(QApplication::translate("MainWindow", "\346\255\243\347\264\257\347\247\257\346\265\201\351\207\217", nullptr));
        label_21->setText(QApplication::translate("MainWindow", "\350\275\254\351\200\237\350\256\276\345\256\232", nullptr));
        label_22->setText(QApplication::translate("MainWindow", "0rpm", nullptr));
        label_23->setText(QApplication::translate("MainWindow", "5000rpm", nullptr));
        label_24->setText(QApplication::translate("MainWindow", "2200rpm", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
