/********************************************************************************
** Form generated from reading UI file 'serial.ui'
**
** Created by: Qt User Interface Compiler version 5.12.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SERIAL_H
#define UI_SERIAL_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_serial
{
public:
    QLabel *label;
    QComboBox *comboBox_2;
    QLabel *label_6;
    QComboBox *comboBox;
    QLabel *label_2;
    QLabel *label_3;
    QComboBox *comboBox_4;
    QPushButton *pushButton;
    QComboBox *comboBox_5;
    QLabel *label_4;
    QComboBox *comboBox_3;

    void setupUi(QDialog *serial)
    {
        if (serial->objectName().isEmpty())
            serial->setObjectName(QString::fromUtf8("serial"));
        serial->resize(650, 500);
        serial->setStyleSheet(QString::fromUtf8("background-color: rgba(42, 47, 59, 1);"));
        label = new QLabel(serial);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(60, 40, 81, 31));
        QFont font;
        font.setFamily(QString::fromUtf8("Arial"));
        font.setPointSize(14);
        label->setFont(font);
        label->setStyleSheet(QString::fromUtf8("background-color: rgba(27, 30, 39, 1);\n"
"color: rgb(255, 255, 255);"));
        label->setAlignment(Qt::AlignCenter);
        comboBox_2 = new QComboBox(serial);
        comboBox_2->addItem(QString());
        comboBox_2->addItem(QString());
        comboBox_2->addItem(QString());
        comboBox_2->addItem(QString());
        comboBox_2->addItem(QString());
        comboBox_2->addItem(QString());
        comboBox_2->addItem(QString());
        comboBox_2->addItem(QString());
        comboBox_2->addItem(QString());
        comboBox_2->addItem(QString());
        comboBox_2->addItem(QString());
        comboBox_2->setObjectName(QString::fromUtf8("comboBox_2"));
        comboBox_2->setGeometry(QRect(190, 110, 92, 28));
        comboBox_2->setFont(font);
        comboBox_2->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);\n"
"border-color: rgb(255, 255, 255);"));
        comboBox_2->setInsertPolicy(QComboBox::InsertAtCurrent);
        comboBox_2->setSizeAdjustPolicy(QComboBox::AdjustToContents);
        label_6 = new QLabel(serial);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(60, 370, 81, 31));
        label_6->setFont(font);
        label_6->setStyleSheet(QString::fromUtf8("background-color: rgba(27, 30, 39, 1);\n"
"color: rgb(255, 255, 255);"));
        label_6->setAlignment(Qt::AlignCenter);
        comboBox = new QComboBox(serial);
        comboBox->setObjectName(QString::fromUtf8("comboBox"));
        comboBox->setGeometry(QRect(190, 40, 121, 31));
        comboBox->setFont(font);
        comboBox->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);"));
        label_2 = new QLabel(serial);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(60, 110, 81, 31));
        label_2->setFont(font);
        label_2->setStyleSheet(QString::fromUtf8("background-color: rgba(27, 30, 39, 1);\n"
"color: rgb(255, 255, 255);"));
        label_2->setAlignment(Qt::AlignCenter);
        label_3 = new QLabel(serial);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(60, 200, 81, 31));
        label_3->setFont(font);
        label_3->setStyleSheet(QString::fromUtf8("background-color: rgba(27, 30, 39, 1);\n"
"color: rgb(255, 255, 255);"));
        label_3->setAlignment(Qt::AlignCenter);
        comboBox_4 = new QComboBox(serial);
        comboBox_4->addItem(QString());
        comboBox_4->addItem(QString());
        comboBox_4->setObjectName(QString::fromUtf8("comboBox_4"));
        comboBox_4->setGeometry(QRect(190, 280, 121, 31));
        comboBox_4->setFont(font);
        comboBox_4->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);"));
        pushButton = new QPushButton(serial);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(450, 210, 131, 51));
        pushButton->setFont(font);
        pushButton->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);"));
        comboBox_5 = new QComboBox(serial);
        comboBox_5->addItem(QString());
        comboBox_5->addItem(QString());
        comboBox_5->addItem(QString());
        comboBox_5->setObjectName(QString::fromUtf8("comboBox_5"));
        comboBox_5->setGeometry(QRect(190, 370, 121, 31));
        comboBox_5->setFont(font);
        comboBox_5->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);\n"
""));
        label_4 = new QLabel(serial);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(60, 280, 81, 31));
        label_4->setFont(font);
        label_4->setStyleSheet(QString::fromUtf8("background-color: rgba(27, 30, 39, 1);\n"
"color: rgb(255, 255, 255);"));
        label_4->setAlignment(Qt::AlignCenter);
        comboBox_3 = new QComboBox(serial);
        comboBox_3->addItem(QString());
        comboBox_3->setObjectName(QString::fromUtf8("comboBox_3"));
        comboBox_3->setGeometry(QRect(190, 200, 121, 31));
        comboBox_3->setFont(font);
        comboBox_3->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 255);"));

        retranslateUi(serial);

        comboBox_2->setCurrentIndex(3);


        QMetaObject::connectSlotsByName(serial);
    } // setupUi

    void retranslateUi(QDialog *serial)
    {
        serial->setWindowTitle(QApplication::translate("serial", "Dialog", nullptr));
        label->setText(QApplication::translate("serial", "\344\270\262\345\217\243\351\200\211\346\213\251", nullptr));
        comboBox_2->setItemText(0, QApplication::translate("serial", "1200", nullptr));
        comboBox_2->setItemText(1, QApplication::translate("serial", "2400", nullptr));
        comboBox_2->setItemText(2, QApplication::translate("serial", "4800", nullptr));
        comboBox_2->setItemText(3, QApplication::translate("serial", "9600", nullptr));
        comboBox_2->setItemText(4, QApplication::translate("serial", "19200", nullptr));
        comboBox_2->setItemText(5, QApplication::translate("serial", "38400", nullptr));
        comboBox_2->setItemText(6, QApplication::translate("serial", "57600", nullptr));
        comboBox_2->setItemText(7, QApplication::translate("serial", "115200", nullptr));
        comboBox_2->setItemText(8, QApplication::translate("serial", "230400", nullptr));
        comboBox_2->setItemText(9, QApplication::translate("serial", "460800", nullptr));
        comboBox_2->setItemText(10, QApplication::translate("serial", "921600", nullptr));

        comboBox_2->setCurrentText(QApplication::translate("serial", "9600", nullptr));
        label_6->setText(QApplication::translate("serial", "\346\240\241\351\252\214\344\275\215", nullptr));
        label_2->setText(QApplication::translate("serial", "\346\263\242\347\211\271\347\216\207", nullptr));
        label_3->setText(QApplication::translate("serial", "\346\225\260\346\215\256\344\275\215", nullptr));
        comboBox_4->setItemText(0, QApplication::translate("serial", "1", nullptr));
        comboBox_4->setItemText(1, QApplication::translate("serial", "2", nullptr));

        pushButton->setText(QApplication::translate("serial", "\346\211\223\345\274\200\344\270\262\345\217\243", nullptr));
        comboBox_5->setItemText(0, QApplication::translate("serial", "None", nullptr));
        comboBox_5->setItemText(1, QApplication::translate("serial", "Even", nullptr));
        comboBox_5->setItemText(2, QApplication::translate("serial", "Odd", nullptr));

        label_4->setText(QApplication::translate("serial", "\345\201\234\346\255\242\344\275\215", nullptr));
        comboBox_3->setItemText(0, QApplication::translate("serial", "8", nullptr));

        comboBox_3->setCurrentText(QApplication::translate("serial", "8", nullptr));
    } // retranslateUi

};

namespace Ui {
    class serial: public Ui_serial {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SERIAL_H
