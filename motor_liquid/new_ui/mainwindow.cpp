#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "windows.h"
#include "string.h"
int current_motor_speed;
static uint16_t CRC16_MudBus(uint8_t *puchMsg, uint8_t usDataLen) {

    uint16_t uCRC = 0xffff;
    for (int n = 0; n < usDataLen; n++) {

        uCRC = puchMsg[n] ^ uCRC;
        for (int i = 0; i < 8; i++) {
            if (uCRC & 0x01) {
                uCRC = uCRC >> 1;
                uCRC = uCRC ^ 0xa001;
            }
            else {
                uCRC = uCRC >> 1;
            }
        }
    }
    return  uCRC;
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QStringList value1;
    QString txt;
    QDateTime data_time = QDateTime::currentDateTime();
    tick_timer = new QTimer;
    file_timer = new QTimer;
    monitor_delay = new QTimer;
    connect(tick_timer, SIGNAL(timeout()), this, SLOT(tick_timer_Update()));
    connect(file_timer, SIGNAL(timeout()), this, SLOT(m_file_update()));
    connect(monitor_delay, SIGNAL(timeout()), this, SLOT(monitor_timer_update()));
    connect(&m_serial, SIGNAL(rev_data(char, char*, int)), this, SLOT(data_rev_Update(char, char*, int)));
    connect(&m_serial, SIGNAL(start_monitor(void)), this, SLOT(start_monitor_timer()));
    tick_timer->start(500);
    ui->label_2->setText(data_time.toString("yyyy年MM月dd日    hh:mm:ss"));

    param_init();
    txt = QString::number(ui->horizontalSlider->value());
    txt += "rpm";
    ui->label_24->setText(txt);
    current_motor_speed = ui->horizontalSlider->value() / 5;
    ui->pushButton_4->setEnabled(false);
}
void MainWindow::param_init(void)
{
    light_param.so2_value = m_setting.param[MIN_SO2];
    light_param.hct_value = m_setting.param[MIN_HCT];
    light_param.hgb_value = m_setting.param[MIN_HGB];
    light_param.po2_value = m_setting.param[MIN_PO2];
    motor_param.motor_speed = 0;
    motor_param.liquid_value = 0;
    motor_param.current_value = 0;
    memset(buttion_data, 0, sizeof(buttion_data));
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::start_monitor_timer(void)
{
    monitor_delay->start(1000);
    file_timer->start(5000);
}
void MainWindow::monitor_timer_update()
{
    QStringList value1;
    static int av_cnt[MAX_TYPE] = {0};
    monitor_delay->stop();
    data_mutex.lock();
    value1 = QString(ui->label_25->text()).split("℃");
    if ((temp1_value > m_setting.param[MAX_T]) || (temp1_value < m_setting.param[MIN_T]))
    {
        av_cnt[TEMP1]++;
        if (av_cnt[TEMP1] > MAX_ALARM_CNT)
        {
            ui->label_25->setStyleSheet("color:red;");
            error_code |= (1 << 0);
        }

    }
    else
    {
        av_cnt[TEMP1] = 0;
        ui->label_25->setStyleSheet("color: rgb(79, 255, 255);");
        error_code &= ~(1 << 0);
    }
   // value1 = QString(ui->label_26->text()).split("℃");
    if ((temp2_value > m_setting.param[MAX_T]) || (temp2_value < m_setting.param[MIN_T]))
    {
         av_cnt[TEMP2]++;
         if (av_cnt[TEMP2] > MAX_ALARM_CNT)
         {
             ui->label_26->setStyleSheet("color:red;");
             error_code |= (1 << 1);

         }
    }
    else
    {
        av_cnt[TEMP2] = 0;
        ui->label_26->setStyleSheet("color: rgb(79, 255, 255);");
        error_code &= ~(1 << 1);
    }
    if (motor_param.motor_speed > m_setting.param[MAX_SPEED] || motor_param.motor_speed < m_setting.param[MIN_SPEED])
    {
        av_cnt[SPEED_TYPE]++;
        if (av_cnt[SPEED_TYPE] > MAX_ALARM_CNT)
        {
            ui->label_8->setStyleSheet("color:red;background-color: rgb(0, 0, 0);");
            error_code |= (1 << 2);
        }
    }
    else
    {
        av_cnt[SPEED_TYPE] = 0;
        ui->label_8->setStyleSheet("color:white;background-color: rgb(0, 0, 0);");
        error_code &= ~(1 << 2);
    }
    if (motor_param.liquid_value > m_setting.param[MAX_LIQUID] || motor_param.liquid_value < m_setting.param[MIN_LIQUID])
    {

        av_cnt[LIQUID_TYPE]++;
        if (av_cnt[LIQUID_TYPE] > MAX_ALARM_CNT)
        {
            ui->label_10->setStyleSheet("color:red;background-color: rgb(0, 0, 0);");
            error_code |= (1 << 3);
        }
    }
    else
    {
        av_cnt[LIQUID_TYPE] = 0;
        ui->label_10->setStyleSheet("color:white;background-color: rgb(0, 0, 0);");
        error_code &= ~(1 << 3);
    }
    if (motor_param.current_value > m_setting.param[MAX_CURRENT] || motor_param.current_value < m_setting.param[MIN_CURRENT])
    {
        av_cnt[CURRENT_TYPE]++;
        if (av_cnt[CURRENT_TYPE] > MAX_ALARM_CNT)
        {
            ui->label_27->setStyleSheet("color:red;background-color: rgb(0, 0, 0);");
            error_code |= (1 << 4);
        }
    }
    else
    {
        av_cnt[CURRENT_TYPE] = 0;
        ui->label_27->setStyleSheet("color:white;background-color: rgb(0, 0, 0);");
        error_code &= ~(1 << 4);
    }
    if (light_param.po2_value > m_setting.param[MAX_PO2] || light_param.po2_value < m_setting.param[MIN_PO2])
    {
        ui->label_29->setStyleSheet("color:red;background-color: rgb(0, 0, 0);");
        error_code |= (1 << 5);
    }
    else
    {
        ui->label_29->setStyleSheet("color:white;background-color: rgb(0, 0, 0);");
        error_code &= ~(1 << 5);
    }
    if (light_param.so2_value > m_setting.param[MAX_SO2] || light_param.so2_value < m_setting.param[MIN_SO2])
    {
        ui->label_30->setStyleSheet("color:red;background-color: rgb(0, 0, 0);");
        error_code |= (1 << 6);
    }
    else
    {
        ui->label_30->setStyleSheet("color:white;background-color: rgb(0, 0, 0);");
        error_code &= ~(1 << 6);
    }
    if (light_param.hct_value > m_setting.param[MAX_HCT] || light_param.hct_value < m_setting.param[MIN_HCT])
    {
        ui->label_28->setStyleSheet("color:red;background-color: rgb(0, 0, 0);");
        error_code |= (1 << 7);
    }
    else
    {
        ui->label_28->setStyleSheet("color:white;background-color: rgb(0, 0, 0);");
        error_code &= ~(1 << 7);
    }
    if (light_param.hgb_value > m_setting.param[MAX_HGB] || light_param.hgb_value < m_setting.param[MIN_HGB])
    {
        ui->label_31->setStyleSheet("color:red;background-color: rgb(0, 0, 0);");
        error_code |= (1 << 8);
    }
    else
    {
        ui->label_31->setStyleSheet("color:white;background-color: rgb(0, 0, 0);");
        error_code &= ~(1 << 8);
    }
    if (error_code != 0)
    {
        char buffer[10] = {0};
        sprintf((char*)buffer, "AC");
        ui->pushButton_4->setEnabled(true);
        ui->label_4->setText("报警");
        ui->label_4->setStyleSheet("color:red;");
        ui->label_3->setPixmap(QPixmap(":/image1/iwarn.png"));
        m_serial.serial_send_data(buffer, strlen((char*)buffer));
    }
    else
    {
        char buffer[10] = {0};
        sprintf((char*)buffer, "AD");
        ui->pushButton_4->setEnabled(false);
        ui->label_4->setText("正常工作");
        ui->label_4->setStyleSheet("color:white;");
        ui->label_3->setPixmap(QPixmap(":/image1/iwarn2.png"));
        m_serial.serial_send_data(buffer, strlen((char*)buffer));
    }
    data_mutex.unlock();
    monitor_delay->start(1000);
}

void MainWindow::tick_timer_Update(void)
{
    static char dev_cnt = 0;

    tick_cnt++;
    dev_cnt++;

    tick_timer->stop();
    data_mutex.lock();
    if (tick_cnt == 2)
    {
        tick_cnt = 0;
        QDateTime data_time = QDateTime::currentDateTime();
        ui->label_2->setText(data_time.toString("yyyy年MM月dd日    hh:mm:ss"));
    }

    if (dev_cnt == 1)
    {
        send_data(C_MOTOR_SPEED);
    }
    else if (dev_cnt == 2)
    {
        send_data(C_TEMP);
    }
    else if (dev_cnt == 3)
    {
        send_data(C_RATED_CURRENT);
    }
    else if (dev_cnt == 4)
    {
        send_data(C_FLOW);
    }

    else if (buttion_data[C_SPEED - 1] == true && dev_cnt == 5)
    {
        send_data(C_SPEED);
    }
    else if (buttion_data[C_ADD - 1] == true && dev_cnt == 6)
    {
        dev_cnt = 0;
        send_data(C_ADD);
    }
    else if (dev_cnt == 6)
    {
        dev_cnt = 0;
    }
    data_mutex.unlock();
    tick_timer->start(500);  

}
void MainWindow::on_pushButton_clicked()
{
    m_serial.show();
}

void MainWindow::on_pushButton_3_clicked()
{
    m_setting.show();
}


void MainWindow::on_pushButton_5_clicked()
{

    buttion_data[C_FLOW - 1] = true;

}

void MainWindow::on_pushButton_6_clicked()
{
    buttion_data[C_FLOW - 1] = false;
    ui->label_15->clear();
}
void MainWindow::m_file_update()
{
    QByteArray buf;
    QString show_data;
    static int last_file_lenth[2] = {0};
    static int current_file_lenth[2] = {0};
    QStringList value1, value2;

    file_timer->stop();

    QFile file_r("C:\\Data\\Y_O2.txt");
    QFile file_r2("C:\\Data\\HCT.txt");
    if (!file_r.exists())
    {
        qDebug() << "failed";
    }
    if (!file_r.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug() << "open failed";
    }

    current_file_lenth[0] = file_r.size();
    current_file_lenth[1] = file_r2.size();


//    if (last_file_lenth[0] == 0)
//    {
//        last_file_lenth[0] = current_file_lenth[0];
//        if (last_file_lenth[1] == 0)
//        {
//            last_file_lenth[1] = current_file_lenth[1];
//        }
//         //return;
//    }
     if (last_file_lenth[0] == current_file_lenth[0])
    {
        //提醒
        return;
    }

    file_r.seek(last_file_lenth[0]);
    buf = file_r.readLine();
    last_file_lenth[0] = current_file_lenth[0];

    value1 = QString(buf).split(";");
    value1[1].replace("\n", "");
  //  qDebug() << "buf1";
   // qDebug() << value1[0];
    ui->label_29->setText("PO2:" + value1[0] + "kPa");
    ui->label_30->setText("SO2:" + value1[1] + "%");
  //  qDebug() << "buf2";
  //  qDebug() << value1[1];
    light_param.po2_value = value1[0].toFloat();
    light_param.so2_value = value1[1].toFloat();


    file_r.close();

    if (!file_r2.exists())
    {
        qDebug() << "failed";
    }
    if (!file_r2.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        qDebug() << "open failed";
    }
    if (last_file_lenth[1] == current_file_lenth[1])
    {
        //提醒

        return;
    }
    file_r2.seek(last_file_lenth[1]);
    buf = file_r2.readLine();
    last_file_lenth[1] = current_file_lenth[1];
    value2 = QString(buf).split(";");
    value2[1].replace("\n", "");
    ui->label_28->setText("HCT:" + value2[0] + "%");
    ui->label_31->setText("Hgb:" + value2[1] + "g/dl");
    light_param.hct_value = value2[0].toFloat();
    light_param.hgb_value = value2[1].toFloat();

    file_r2.close();
   // file_timer->start(5000);

}
static int find_motor_data(const char* buffer, uint16_t lenth)
{
    uint16_t cnt = 0;

    while (cnt < lenth)
    {
        if ((buffer[cnt] == 0x01) && (buffer[cnt + 1] == 0x03))
        {
            return cnt;
        }
        else
        {
            qDebug("error1 = %d, error2 = %d", buffer[cnt], buffer[cnt + 1]);
            cnt++;
        }
    }
    qDebug("cnt = %d", cnt);
    return -1;
}
void MainWindow::data_rev_Update(char type, char* data, int lenth)
{
    QStringList value1;

    float data_liquid;
    int num;
    data_mutex.lock();
    if (type == C_FLOW)
    {
        qDebug() << "c_flow" << data[0];
        if (QString(&data[1]).indexOf("E") == -1)
        {
            data_mutex.unlock();
            return;
        }

        value1 = QString(&data[1]).split("E");
        if (value1[0] == NULL)
        {
            data_mutex.unlock();
            return;
        }

        num = 10 * value1[1].data()[1].digitValue() + value1[1].data()[2].digitValue();

        if (num == 0)
        {
            data_liquid = value1[0].toFloat();
        }
        else
        {
            data_liquid = value1[0].toFloat() / (10 * num);

        }
        data_liquid = data_liquid * 1000 / 60;
        ui->label_15->clear();
        if (buttion_data[C_FLOW - 1] == true)
        {
            ui->label_15->setText(QString::number(data_liquid));
        }
        ui->label_10->setText("流量:" + QString::number(data_liquid));

        motor_param.liquid_value = data_liquid;
    }
    else if (type == C_SPEED)
    {
        qDebug() << "c_speed" << data;
        if (QString(&data[1]).indexOf("E") == -1)
        {
            data_mutex.unlock();
            return;

        }
        value1 = QString(&data[1]).split("E");
        if (value1[0] == NULL)
        {
            data_mutex.unlock();
            return;
        }

        num = 10 * value1[1].data()[1].digitValue() + value1[1].data()[2].digitValue();
        if (num == 0)
        {
            data_liquid = value1[0].toFloat();
        }
        else
        {
            data_liquid = value1[0].toFloat() / (10 * num);

        }

        ui->label_16->clear();
        ui->label_16->setText(QString::number(data_liquid));
    }
    else if (type == C_ADD)
    {
        float data_buffer;
        qDebug() << "c_add" << data;
        if (QString(&data[1]).indexOf("E") == -1)
        {
             data_mutex.unlock();
             return;
        }
        value1 = QString(&data[1]).split("E");
        if (value1[0] == NULL)
        {
            data_mutex.unlock();
            return;
        }

        num = value1[1].data()[1].digitValue();
        if (num == 0)
        {
            data_buffer = value1[0].toFloat();
        }
        else
        {
            data_buffer = value1[0].toFloat() / (10 * num);
        }

        ui->label_17->clear();
        ui->label_17->setText(QString::number(data_buffer));
    }
    else if (type == C_MOTOR_SPEED)
    {
        qDebug() << "C_MOTOR_SPEED";
         QString text;
        int motor_data_pos = find_motor_data(data, strlen((char*)data));
        int data_lenth = strlen((char*)data);
        qDebug() << "motor_lenth" << motor_data_pos << data_lenth;
        if (motor_data_pos == -1)
        {
            qDebug() << "data_lenth" << data_lenth;
             data_mutex.unlock();
            return;
        }
        else if (data_lenth <= motor_data_pos + 4) //数据丢失
        {
            qDebug() << "data_miss";
            ui->label_8->setText("转速:" + ui->label_24->text());
            data_mutex.unlock();
            return;
        }
        motor_param.motor_speed = ((uint8_t)data[motor_data_pos + 3] << 8) + (uint8_t)data[motor_data_pos + 4];

        text = QString::number((float)motor_param.motor_speed);
        ui->label_8->setText("转速:" + text);
    }
    else if (type == C_TEMP)
    {
        qDebug() << "C_TEMP";
        QStringList value;
        if (lenth < 5)
        {
            data_mutex.unlock();
            return;
        }
        value = QString(data).split(",");
        if (value[1] == NULL || value[2] == NULL)
        {
            data_mutex.unlock();
            return;
        }

        temp1_value = value[1].toUInt();
        temp2_value = value[2].toUInt();

        if (temp1_value >= 0 && temp2_value >= 0)
        {
            ui->label_25->clear();
            ui->label_25->setText(value[1] + "℃");
            ui->label_26->clear();
            ui->label_26->setText(value[2] + "℃");
        }
        else
        {
             data_mutex.unlock();
            return;
        }

    }
    else if (type == C_RATED_CURRENT)
    {
        qDebug() << "C_RATED_CURRENT";
        int motor_data_pos = find_motor_data(data, strlen((char*)data));
        int data_lenth = lenth;
        qDebug() << "current_lenth" << motor_data_pos << data_lenth;
        if (motor_data_pos == -1)
        {
            qDebug() << "data_lenth" << data[0];
            data_mutex.unlock();
            return;
        }
        else if (data_lenth <= motor_data_pos + 4) //数据丢失
        {
            qDebug() << "current_data_miss";
            qDebug("%d, %d, %d", data[0], data[1], data[2]);
            data_mutex.unlock();
            return;
        }
        motor_param.current_value = ((uint8_t)data[motor_data_pos + 3] << 8) + (uint8_t)data[motor_data_pos + 4];
        motor_param.current_value = motor_param.current_value / 100;
        qDebug() << motor_param.current_value;
    }
    data_mutex.unlock();

}
void MainWindow::read_motor_data(uint16_t addr, uint8_t num)
{
    uint8_t send_data[RS_485_W_MAX] = {0};
    uint16_t temp;
    send_data[RS_485_ADDR] = 1;
    send_data[C_DATA] = 0X03;
    send_data[DATA_ADDR_H] = addr >> 8;
    send_data[DATA_ADDR_L] = addr;
    send_data[DATA_H] = num >> 8;
    send_data[DATA_L] = num;

    temp = CRC16_MudBus(send_data, CRC_DATA_L);
    send_data[CRC_DATA_L] = temp;
    send_data[CRC_DATA_H] = temp >> 8;
    m_serial.serial_send_data((char*)send_data, RS_485_W_MAX);

}
void MainWindow::write_motor_data(uint16_t addr, uint16_t data)
{
    uint8_t send_data[RS_485_W_MAX] = {0};
    uint16_t temp;

    send_data[RS_485_ADDR] = 1;
    send_data[C_DATA] = 0x06;
    send_data[DATA_ADDR_H] = addr >> 8;
    send_data[DATA_ADDR_L] = addr;
    send_data[DATA_H] = data >> 8;
    send_data[DATA_L] = data;

    temp = CRC16_MudBus(send_data, CRC_DATA_L);

    send_data[CRC_DATA_L] = temp;
    send_data[CRC_DATA_H] = temp >> 8;

    m_serial.serial_send_data((char*)send_data, RS_485_W_MAX);
}


void MainWindow::send_data(int type)
{
    char buffer[10] = {0};

   // data_mutex.lock();

    if (C_FLOW == type)
    {
        sprintf((char*)buffer, "RFR\r\n");
        m_serial.motor_rev_flag = C_FLOW;
    }
    else if (type == C_SPEED)
    {
        sprintf((char*)buffer, "RVV\r\n");
        m_serial.motor_rev_flag = C_SPEED;
    }
    else if (type == C_ADD)
    {
        sprintf((char*)buffer, "RT+\r\n");
        m_serial.motor_rev_flag = C_ADD;
    }
    else if (type == C_MOTOR_SPEED)
    {
        m_serial.motor_rev_flag = C_MOTOR_SPEED;
        read_motor_data(0x0034, 1);
        return;
    }
    else if (type == C_TEMP)
    {
        m_serial.motor_rev_flag = C_TEMP;
        sprintf((char*)buffer, "AB");
    }
    else if (type == C_RATED_CURRENT)
    {
        m_serial.motor_rev_flag = C_RATED_CURRENT;
        read_motor_data(0x0021, 1);
        return;
    }

    if (m_serial.motor_rev_flag == 0) return;
    m_serial.serial_send_data(buffer, strlen(buffer));

   // data_mutex.unlock();
}

void MainWindow::on_pushButton_7_clicked()
{
    m_speed_flag = true;
    buttion_data[C_SPEED - 1] = true;
    send_data(C_SPEED);
}

void MainWindow::on_horizontalSlider_valueChanged(int value)
{
    QString txt;
    txt = QString::number(value);
    txt += "rpm";
    ui->label_24->setText(txt);
    current_motor_speed = value / 5;
}

void MainWindow::on_pushButton_8_clicked()
{
    m_speed_flag = false;
    buttion_data[C_SPEED - 1] = false;
    ui->label_16->clear();

}

void MainWindow::on_pushButton_9_clicked()
{
    m_add_flag = true;
    buttion_data[C_ADD - 1] = true;
    send_data(C_ADD);
}

void MainWindow::on_pushButton_10_clicked()
{
    m_add_flag = false;
    buttion_data[C_ADD - 1] = false;
    ui->label_17->clear();
}

void MainWindow::on_horizontalSlider_sliderReleased()
{
    write_motor_data(0x0042, current_motor_speed);
}

void MainWindow::on_pushButton_4_clicked()
{
    char buffer[10] = {0};
    sprintf((char*)buffer, "AE");
    m_serial.serial_send_data(buffer, strlen(buffer));
}

void MainWindow::on_pushButton_2_clicked()
{
    m_serial.motor_rev_flag = 0;
    write_motor_data(0x0040, 0);
    ui->horizontalSlider->setValue(0);
    ui->label_24->setText(QString::number(0) + "rpm");
}
