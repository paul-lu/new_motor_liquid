#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "serial.h"
#include <QThread>
#include <QMutexLocker>
#include <QMutex>
#include "setting.h"


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE


#define T_DLEAY 100
#define DATA_DLEAY 50
#define MOTOR_DLEAY 200
#define MAX_ALARM_CNT 2

typedef struct _motor_param
{
    int motor_speed;
    float liquid_value;
    float current_value;

}motor_param_t;

typedef struct _motor_flag
{
   bool m_speed_flag;
   bool m_motor_flag;
   bool m_current_flag;

}motor_flag_t;

typedef struct _light_param
{
    float so2_value;
    float po2_value;
    float hct_value;
    float hgb_value;

}light_param_t;

enum
{
    RS_485_ADDR = 0,
    C_DATA,
    DATA_ADDR_H,
    DATA_ADDR_L,
    DATA_H,
    DATA_L,
    CRC_DATA_L,
    CRC_DATA_H,
    RS_485_W_MAX,
};

enum
{
    C_MOTOR_SPEED = 1,  //电机转速
    C_TEMP,
    C_RATED_CURRENT,
    C_FLOW, //瞬时流量
    C_SPEED, //
    C_ADD,
    C_MAX,
};

enum
{
    TEMP1 = 0,
    TEMP2,
    SPEED_TYPE,
    LIQUID_TYPE,
    CURRENT_TYPE,
    PO2_TYPE,
    SO2_TYPE,
    HCT_TYPE,
    HGB_TYPE,
    MAX_TYPE,

};


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    int tick_cnt = 0;
    int file_read_cnt = 0;
    //int motor_speed;
    setting m_setting;
    serial m_serial;
    QTimer* tick_timer;
    QTimer* monitor_delay;
    QTimer* file_timer;
    bool m_flow_flag;
    bool m_speed_flag;
    bool m_add_flag;
    bool buttion_data[C_MAX - 1];
    char dev_cnt;
    uint16_t error_code;
    motor_param_t motor_param;

    QMutex data_mutex;
    uint8_t temp1_value;
    uint8_t temp2_value;
   // motor_flag_t motor_flag;
    light_param_t light_param;
    void read_motor_data(uint16_t addr, uint8_t num);
    void write_motor_data(uint16_t addr, uint16_t data);
    void write_motor_num_data(uint16_t addr, uint16_t num, uint16_t* data);
    void send_data(int);
    void param_init(void);
private slots:
    void on_pushButton_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_6_clicked();

    void tick_timer_Update();

    void m_file_update();

    void data_rev_Update(char, char*, int);

    void start_monitor_timer(void);

     void monitor_timer_update();
     void on_pushButton_7_clicked();

     void on_horizontalSlider_valueChanged(int value);

     void on_pushButton_8_clicked();

     void on_pushButton_9_clicked();

     void on_pushButton_10_clicked();

     void on_horizontalSlider_sliderReleased();

     void on_pushButton_4_clicked();

     void on_pushButton_2_clicked();

private:
    Ui::MainWindow *ui;

signals:

};
#endif // MAINWINDOW_H
