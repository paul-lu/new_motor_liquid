#include "serial.h"
#include "ui_serial.h"
#include <string.h>
#include "mainwindow.h"
#include <string.h>

QByteArray   buf;
static int m_usart_flag = 0;




serial::serial(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::serial)
{
//     uint8_t data_buf[20] = {0};
//     uint8_t buffer[2] = {22, 34};
    ui->setupUi(this);
    serial_port = new QSerialPort;
    uart_delay = new QTimer(this);
    connect(uart_delay, SIGNAL(timeout()), this, SLOT(TimerUpdate()));

    scanSerialPort_handler();
    connect(serial_port, &QSerialPort::readyRead, this, &serial::serialPortReadyRead);
    setWindowTitle("串口配置");

//    sprintf((char*)data_buf, "%s,%d,%d", "temp", buffer[0], buffer[1]);
//    qDebug("%s", data_buf);
//    qDebug("%d", strlen((char*)data_buf));

}

serial::~serial()
{
    delete ui;
}


void serial::TimerUpdate()
{
    char* s_buffer = {0};
    int data_lenth = 0;
    uart_delay->stop();
    data_lenth = buf.length();

    if (data_lenth != 0)
    {
        s_buffer = buf.data();
        if (motor_rev_flag == C_TEMP)
        {
             if (strncmp(s_buffer, "temp", 4) == 0)
             {
                 emit rev_data(motor_rev_flag, s_buffer, data_lenth);
             }
        }
        else if (motor_rev_flag != 0 && motor_rev_flag != C_TEMP)
        {
             emit rev_data(motor_rev_flag, s_buffer, data_lenth);
        }
    }

    buf.clear();
    uart_delay->start(200);
}

void serial::scanSerialPort_handler()
{
     /* 查找可用串口 */
     foreach (const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
     {
        ui->comboBox->addItem(info.portName());
     }
}

void serial::serialPortReadyRead()
{
    uart_delay->start(100);
    buf.append(serial_port->readAll());
}

void serial::serial_send_data(char* data, int lenth)
{
   // qDebug() << m_usart_flag;
    if (m_usart_flag == 1)
    {
        send_flag = true;
        serial_port->write(data, lenth);

    }
}

void serial::on_pushButton_clicked()
{
    if (ui->pushButton->text() == "打开串口")
    {
        serial_port->setPortName(ui->comboBox->currentText());
        serial_port->setBaudRate(ui->comboBox_2->currentText().toInt());
        if(ui->comboBox_3->currentText().toInt() == 5)
        {
            serial_port->setDataBits(QSerialPort::Data5);
        }
        else if (ui->comboBox_3->currentText().toInt() == 6) {
            serial_port->setDataBits(QSerialPort::Data6);
        }
        else if (ui->comboBox_3->currentText().toInt() == 7) {
            serial_port->setDataBits(QSerialPort::Data7);
        }
        else if (ui->comboBox_3->currentText().toInt() == 8) {
            serial_port->setDataBits(QSerialPort::Data8);
        }

        switch (ui->comboBox_5->currentIndex()) {
         case 0:
         serial_port->setParity(QSerialPort::NoParity);
         break;
         case 1:
        serial_port->setParity(QSerialPort::EvenParity);
         break;
         case 2:
         serial_port->setParity(QSerialPort::OddParity);
        break;

         default: break;
         }
        if (ui->comboBox_4->currentText().toInt() == 1)
        {
            serial_port->setStopBits(QSerialPort::OneStop);
        }
        else
        {
            serial_port->setStopBits(QSerialPort::TwoStop);
        }
        /* 设置流控制 */
        serial_port->setFlowControl(QSerialPort::NoFlowControl);
        if (!serial_port->open(QIODevice::ReadWrite))
        {
            QMessageBox::about(NULL, "错误","串口无法打开！可能被占用");
        }

        else
        {
            ui->comboBox->setEnabled(false);
            ui->comboBox_2->setEnabled(false);
            ui->comboBox_3->setEnabled(false);
            ui->comboBox_4->setEnabled(false);
            ui->comboBox_5->setEnabled(false);
            ui->pushButton->setText("关闭串口");
            m_usart_flag = 1;
            emit start_monitor();
         }
    }
    else
    {
         serial_port->close();
         ui->comboBox->setEnabled(true);
         ui->comboBox_2->setEnabled(true);
         ui->comboBox_3->setEnabled(true);
         ui->comboBox_4->setEnabled(true);
         ui->comboBox_5->setEnabled(true);
         ui->pushButton->setText("打开串口");
         m_usart_flag = 0;

    }
}
