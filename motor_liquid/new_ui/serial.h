#ifndef SERIAL_H
#define SERIAL_H

#include <QDialog>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QMessageBox>
#include <QFileDialog>
#include <QDebug>
#include <QCheckBox>
#include <QTimer>
#include <QDebug>
#include <QTime>
#include "stdint.h"
namespace Ui {
class serial;
}
typedef struct s_motor_data
{
    uint16_t pwm_rise_time;
    uint16_t pwm_fall_time;
    uint16_t add_acce;
    uint16_t reduce_acce;
    uint16_t rated_current;
    uint16_t motor_speed;

}s_motor_data_t;
class serial : public QDialog
{
    Q_OBJECT

public:
    explicit serial(QWidget *parent = nullptr);
    ~serial();
    bool send_flag = false;
    void serial_send_data(char*, int);
    QString liquid_buffer;
    uint8_t motor_rev_flag = 0;
    s_motor_data_t s_motor_data;
    bool motor_flag;
private:
    //int m_usart_flag = 0;
    Ui::serial *ui;
    QSerialPort* serial_port;
    QTimer* uart_delay;
    void scanSerialPort_handler();
    void serialPortReadyRead();
private slots:
    void on_pushButton_clicked();
    void TimerUpdate();

signals:
    void rev_data(char, char*, int);
    void start_monitor(void);

};

#endif // SERIAL_H
