#include "setting.h"
#include "ui_setting.h"
#include "qtimer.h"
#include "qdatetime.h"
//#include "QDebug.h"
/*
SO2：检测范围：40%-100%，分辨率1%；
Hgb：5g/dL-15g/dL，分辨率：0.1 g/dL；
HCT：检测范围：15%-50%；分辨率：1%；
PO2：检测范围：1.3 kPa -40 kPa，分别率0.1kPa；
*/


setting::setting(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::setting)
{
    ui->setupUi(this);
    tick_timer = new QTimer;
    connect(tick_timer, SIGNAL(timeout()), this, SLOT(tick_timer_Update()));
    file_read_init();
    tick_timer->start(500);
}
void setting::tick_timer_Update()
{
    QDateTime data_time = QDateTime::currentDateTime();
    ui->label_3->setText(data_time.toString("yyyy年MM月dd日    hh:mm:ss"));
}
void setting::file_read_init(void)
{
    QByteArray buf;
    QStringList value1;
    int file_lenth = 0;
    int cnt = 0;
    QFile file_r1("C:\\Data\\data_param.txt");

    if (!file_r1.open(QIODevice::ReadWrite | QIODevice::Text))
    {
        //qDebug() << "open failed";
    }
    file_lenth = file_r1.size();
    if (file_lenth == NULL) return;

    buf = file_r1.readLine();
    value1 = QString(buf).split(";");


    ui->lineEdit->setText(value1[cnt++]);
    ui->lineEdit_2->setText(value1[cnt++]);
    ui->lineEdit_3->setText(value1[cnt++]);
    ui->lineEdit_4->setText(value1[cnt++]);
    ui->lineEdit_5->setText(value1[cnt++]);
    ui->lineEdit_6->setText(value1[cnt++]);
    ui->lineEdit_7->setText(value1[cnt++]);

    ui->lineEdit_12->setText(value1[cnt++]);
    ui->lineEdit_13->setText(value1[cnt++]);

    ui->lineEdit_16->setText(value1[cnt++]);
    ui->lineEdit_17->setText(value1[cnt++]);
    ui->lineEdit_18->setText(value1[cnt++]);
    ui->lineEdit_19->setText(value1[cnt++]);
    ui->lineEdit_20->setText(value1[cnt++]);
    ui->lineEdit_21->setText(value1[cnt++]);
    ui->lineEdit_22->setText(value1[cnt++]);
    ui->lineEdit_23->setText(value1[cnt++]);
    ui->lineEdit_24->setText(value1[cnt++]);
    ui->lineEdit_25->setText(value1[cnt++]);
    ui->lineEdit_26->setText(value1[cnt++]);
    ui->lineEdit_27->setText(value1[cnt++]);

    file_r1.close();

    cnt = 0;
    param[cnt++] = ui->lineEdit->text().toFloat();
    param[cnt++] = ui->lineEdit_2->text().toFloat();
    param[cnt++] = ui->lineEdit_3->text().toFloat();
    param[cnt++] = ui->lineEdit_4->text().toFloat();
    param[cnt++] = ui->lineEdit_5->text().toFloat();
    param[cnt++] = ui->lineEdit_6->text().toFloat();
    param[cnt++] = ui->lineEdit_7->text().toFloat();

    param[cnt++] = ui->lineEdit_12->text().toFloat();
    param[cnt++] = ui->lineEdit_13->text().toFloat();
    param[cnt++] = ui->lineEdit_16->text().toFloat();
    param[cnt++] = ui->lineEdit_17->text().toFloat();
    param[cnt++] = ui->lineEdit_18->text().toFloat();
    param[cnt++] = ui->lineEdit_19->text().toFloat();

    param[cnt++] = ui->lineEdit_20->text().toFloat();
    param[cnt++] = ui->lineEdit_21->text().toFloat();
    param[cnt++] = ui->lineEdit_22->text().toFloat();
    param[cnt++] = ui->lineEdit_23->text().toFloat();

    param[cnt++] = ui->lineEdit_24->text().toFloat();
    param[cnt++] = ui->lineEdit_25->text().toFloat();
    param[cnt++] = ui->lineEdit_26->text().toFloat();
    param[cnt++] = ui->lineEdit_27->text().toFloat();

}

void setting::file_save(void)
{
    int cnt = 0;
    QFile file_r1("C:\\Data\\data_param.txt");
    if (!file_r1.open(QIODevice::ReadWrite | QIODevice::Text))
    {
        // qDebug() << "open failed";
    }

    file_r1.write(ui->lineEdit->text().toLatin1().data());
    file_r1.write(";");
    file_r1.write(ui->lineEdit_2->text().toLatin1().data());
    file_r1.write(";");
    file_r1.write(ui->lineEdit_3->text().toLatin1().data());
    file_r1.write(";");
    file_r1.write(ui->lineEdit_4->text().toLatin1().data());
    file_r1.write(";");
    file_r1.write(ui->lineEdit_5->text().toLatin1().data());
    file_r1.write(";");

    file_r1.write(ui->lineEdit_6->text().toLatin1().data());
    file_r1.write(";");
    file_r1.write(ui->lineEdit_7->text().toLatin1().data());
    file_r1.write(";");
    file_r1.write(ui->lineEdit_12->text().toLatin1().data());
    file_r1.write(";");
    file_r1.write(ui->lineEdit_13->text().toLatin1().data());
    file_r1.write(";");
    file_r1.write(ui->lineEdit_16->text().toLatin1().data());
    file_r1.write(";");
    file_r1.write(ui->lineEdit_17->text().toLatin1().data());
    file_r1.write(";");
    file_r1.write(ui->lineEdit_18->text().toLatin1().data());
    file_r1.write(";");
    file_r1.write(ui->lineEdit_19->text().toLatin1().data());
    file_r1.write(";");

    file_r1.write(ui->lineEdit_20->text().toLatin1().data());
    file_r1.write(";");
    file_r1.write(ui->lineEdit_21->text().toLatin1().data());
    file_r1.write(";");
    file_r1.write(ui->lineEdit_22->text().toLatin1().data());
    file_r1.write(";");
    file_r1.write(ui->lineEdit_23->text().toLatin1().data());
    file_r1.write(";");

    file_r1.write(ui->lineEdit_24->text().toLatin1().data());
    file_r1.write(";");
    file_r1.write(ui->lineEdit_25->text().toLatin1().data());
    file_r1.write(";");
    file_r1.write(ui->lineEdit_26->text().toLatin1().data());
    file_r1.write(";");
    file_r1.write(ui->lineEdit_27->text().toLatin1().data());
    file_r1.write(";");

    file_r1.close();

    param[cnt++] = ui->lineEdit->text().toFloat();
    param[cnt++] = ui->lineEdit_2->text().toFloat();
    param[cnt++] = ui->lineEdit_3->text().toFloat();
    param[cnt++] = ui->lineEdit_4->text().toFloat();
    param[cnt++] = ui->lineEdit_5->text().toFloat();
    param[cnt++] = ui->lineEdit_6->text().toFloat();
    param[cnt++] = ui->lineEdit_7->text().toFloat();

    param[cnt++] = ui->lineEdit_12->text().toFloat();
    param[cnt++] = ui->lineEdit_13->text().toFloat();
    param[cnt++] = ui->lineEdit_16->text().toFloat();
    param[cnt++] = ui->lineEdit_17->text().toFloat();
    param[cnt++] = ui->lineEdit_18->text().toFloat();
    param[cnt++] = ui->lineEdit_19->text().toFloat();

    param[cnt++] = ui->lineEdit_20->text().toFloat();
    param[cnt++] = ui->lineEdit_21->text().toFloat();
    param[cnt++] = ui->lineEdit_22->text().toFloat();
    param[cnt++] = ui->lineEdit_23->text().toFloat();

    param[cnt++] = ui->lineEdit_24->text().toFloat();
    param[cnt++] = ui->lineEdit_25->text().toFloat();
    param[cnt++] = ui->lineEdit_26->text().toFloat();
    param[cnt++] = ui->lineEdit_27->text().toFloat();

}
setting::~setting()
{
    delete ui;
}

static uint16_t CRC16_MudBus(uint8_t *puchMsg, uint8_t usDataLen) {

    uint16_t uCRC = 0xffff;
    for (int n = 0; n < usDataLen; n++) {

        uCRC = puchMsg[n] ^ uCRC;
        for (int i = 0; i < 8; i++) {
            if (uCRC & 0x01) {
                uCRC = uCRC >> 1;
                uCRC = uCRC ^ 0xa001;
            }
            else {
                uCRC = uCRC >> 1;
            }
        }
    }
    return  uCRC;
}
void setting::write_motor_data(uint16_t addr, uint16_t data)
{
    uint8_t send_data[8] = {0};
    uint16_t temp;

    send_data[0] = 1;
    send_data[1] = 0x06;
    send_data[2] = addr >> 8;
    send_data[3] = addr;
    send_data[4] = data >> 8;
    send_data[5] = data;

    temp = CRC16_MudBus(send_data, 6);

    send_data[6] = temp;
    send_data[7] = temp >> 8;

    m_serial.serial_send_data((char*)send_data, 8);
}
void setting::write_motor_num_data(uint16_t addr, uint16_t num, uint16_t *data)
{
    uint8_t send_data[50] = {0};
    uint16_t cnt = 0;
    uint16_t temp;
    send_data[cnt++] = 1;
    send_data[cnt++] = 0x10;
    send_data[cnt++] = addr >> 8;
    send_data[cnt++] = addr;
    send_data[cnt++] = num >> 8;
    send_data[cnt++] = num;

    send_data[cnt++] = num * 2;

    for (int i = 0; i < num; i++)
    {
        send_data[cnt++] = data[i] >> 8;
        send_data[cnt++] = data[i];

    }
    temp = CRC16_MudBus(send_data, cnt);
    send_data[cnt++] = temp;
    send_data[cnt++] = temp >> 8;

    m_serial.serial_send_data((char*)send_data, cnt);
}

void setting::on_pushButton_2_clicked()
{
    uint16_t data[4] = {0};
    file_save();
    //
    data[0] = ui->lineEdit->text().toFloat() * 10;
    data[1] = ui->lineEdit_4->text().toFloat() * 10;
    data[2] = ui->lineEdit_2->text().toFloat() * 10;
    data[3] = ui->lineEdit_5->text().toFloat() * 10;
    write_motor_num_data(0x0050, 4, data);

    data[0] = ui->lineEdit_3->text().toFloat() * 100;

    write_motor_data(0x006a, data[0]);


    this->close();
}

