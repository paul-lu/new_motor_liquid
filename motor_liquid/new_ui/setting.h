#ifndef SETTING_H
#define SETTING_H

#include <QWidget>
#include "qpushbutton.h"
#include "qfile.h"
#include "serial.h"

namespace Ui {
class setting;
}

/*
    MAX_LIQUID,
    MIN_LIQUID,
    MAX_CURRENT,
    MIN_CURRENT,
    MAX_HCT,
    MIN_HCT,
    MAX_PO2,
    MIN_PO2,
    MAX_HGB,
    MIN_HGB,
    MAX_SO2,
    MIN_SO2,

*/

enum
{
    PWM_UP_PARAM = 0,
    ADD_SPEED_PARAM,
    C_CURRENT_PARAM,
    PWM_DOWN_PARAM,
    DEL_SPEED_PARAM,
    MAX_SPEED,
    MIN_SPEED,
    MAX_LIQUID,
    MIN_LIQUID,
    MAX_CURRENT,
    MIN_CURRENT,
    MAX_HCT,
    MIN_HCT,
    MAX_PO2,
    MIN_PO2,
    MAX_HGB,
    MIN_HGB,
    MAX_SO2,
    MIN_SO2,
    MAX_T,
    MIN_T,
    MAX_PARAM,


};

class setting : public QWidget
{
    Q_OBJECT

public:
    explicit setting(QWidget *parent = nullptr);
    ~setting();
    QTimer* tick_timer;
    void file_read_init(void);
    void file_save(void);

    float param[MAX_PARAM];
    serial m_serial;
    void write_motor_num_data(uint16_t addr, uint16_t num, uint16_t *data);
    void write_motor_data(uint16_t addr, uint16_t data);
private slots:
    void on_pushButton_2_clicked();
    void tick_timer_Update();


private:
    Ui::setting *ui;

};

#endif // SETTING_H
